本文分析基于内核版本 6.12.0

[TOC]

# 1. platform数据结构
```c
File: include\linux\platform_device.h
23: struct platform_device {
24: 	const char	*name;
25: 	int		id;
26: 	bool		id_auto;
27: 	struct device	dev;
28: 	u64		platform_dma_mask;
29: 	struct device_dma_parameters dma_parms;
30: 	u32		num_resources;
31: 	struct resource	*resource;
32: 
33: 	const struct platform_device_id	*id_entry;
34: 	/*
35: 	 * Driver name to force a match.  Do not set directly, because core
36: 	 * frees it.  Use driver_set_override() to set or clear it.
37: 	 */
38: 	const char *driver_override;
39: 
40: 	/* MFD cell pointer */
41: 	struct mfd_cell *mfd_cell;
42: 
43: 	/* arch specific additions */
44: 	struct pdev_archdata	archdata;
45: };
```

- name:设备的名称，在设备注册时，该名称会拷贝到dev.init_name中。
- id: 用于标识该设备的ID，如果该值取PLATFORM_DEVID_NONE，则dev的名称将设置为 name 字段的值；如果id取值为PLATFORM_DEVID_AUTO,则dev的名称将设置为 "name.id.auto"，其中name为name字段的值，id为系统自动分配的一个整形值，auto是一个字符串用于标识该名称是系统自动分配；如果id取值为其他值，则dev的名称为 "name.id"，其中id就是id字段的值。

```c
File: include\linux\platform_device.h
236: struct platform_driver {
237: 	int (*probe)(struct platform_device *);
238: 
239: 	/*
240: 	 * .remove_new() is a relic from a prototype conversion of .remove().
241: 	 * New drivers are supposed to implement .remove(). Once all drivers are
242: 	 * converted to not use .remove_new any more, it will be dropped.
243: 	 */
244: 	union {
245: 		void (*remove)(struct platform_device *);
246: 		void (*remove_new)(struct platform_device *);
247: 	};
248: 
249: 	void (*shutdown)(struct platform_device *);
250: 	int (*suspend)(struct platform_device *, pm_message_t state);
251: 	int (*resume)(struct platform_device *);
252: 	struct device_driver driver;
253: 	const struct platform_device_id *id_table;
254: 	bool prevent_deferred_probe;
255: 	/*
256: 	 * For most device drivers, no need to care about this flag as long as
257: 	 * all DMAs are handled through the kernel DMA API. For some special
258: 	 * ones, for example VFIO drivers, they know how to manage the DMA
259: 	 * themselves and set this flag so that the IOMMU layer will allow them
260: 	 * to setup and manage their own I/O address space.
261: 	 */
262: 	bool driver_managed_dma;
263: };
```

# 2. platform模块的初始化
在《machine初始化及platform设备绑定过程.md》文章中就已经介绍过platform的初始化了，具体函数调用流程可以参考该文章。这里我们再看一下：

```c
File: drivers\base\platform.c
1512: int __init platform_bus_init(void)
1513: {
1514: 	int error;
1515: 
1516: 	early_platform_cleanup();
1517: 
1518: 	error = device_register(&platform_bus);
1519: 	if (error) {
1520: 		put_device(&platform_bus);
1521: 		return error;
1522: 	}
1523: 	error =  bus_register(&platform_bus_type);
1524: 	if (error)
1525: 		device_unregister(&platform_bus);
1526: 
1527: 	return error;
1528: }
```

使用基础设施 device_register() 函数注册 platform_bus 设备，该设备定义如下：

```c
File: drivers\base\platform.c
42: struct device platform_bus = {
43: 	.init_name	= "platform",
44: };
```

因为 init_name为"platform"，按照设备模型，注册后会在生成 `/sys/devices/platform/` 目录。

接着调用 bus_register() 函数注册 platform_bus_type 总线类型，该类型定义如下：

```c
File: drivers\base\platform.c
1477: const struct bus_type platform_bus_type = {
1478: 	.name		= "platform",
1479: 	.dev_groups	= platform_dev_groups,
1480: 	.match		= platform_match,
1481: 	.uevent		= platform_uevent,
1482: 	.probe		= platform_probe,
1483: 	.remove		= platform_remove,
1484: 	.shutdown	= platform_shutdown,
1485: 	.dma_configure	= platform_dma_configure,
1486: 	.dma_cleanup	= platform_dma_cleanup,
1487: 	.pm		= &platform_dev_pm_ops,
1488: };
```

然后会创建 `/sys/bus/platform/` 目录，会在该目录下创建 drivers/，devices/ 两个子目录；还会在该目录下创建 drivers_probe, drivers_autoprobe 两个属性文件。(请参考《设备模型device及device_driver分析.md》)

![](./images/Snipaste_2024-12-14_21-17-48.png)

# 3. platform设备的注册
platform device注册调用`platform_device_register()`函数，定义如下：

```c
File: drivers\base\platform.c
775: int platform_device_register(struct platform_device *pdev)
776: {
777: 	device_initialize(&pdev->dev);
778: 	setup_pdev_dma_masks(pdev);
779: 	return platform_device_add(pdev);
780: }
```

其函数调用流程如下图：

![](./images/Snipaste_2024-12-14_21-48-58.png)


# 4. platform驱动的注册
platform驱动注册调用`platform_driver_register()`宏，该宏定义如下：

```c
File: include\linux\platform_device.h
271: #define platform_driver_register(drv) \
272: 	__platform_driver_register(drv, THIS_MODULE)
```

函数调用流程如下图：

![](./images/Snipaste_2024-12-14_22-43-46.png)

在上面流程图中就可以看到如何调用的 platform_driver 的 probe 函数了，并可以了解到会默认创建哪些属性文件。

原图为 "draw/设备模型platform.png"
