本文基于内核版本 6.12.0

[TOC]

# 1.入口 setup_arch
设备树解析的入口函数为 `setup_arch()`。该函数做的事情很多，其中就包括设备树的解析。下面只分析一下与设备树解析相关的部分。

```c
File: arch\arm\kernel\setup.c
1102: void __init setup_arch(char **cmdline_p)
1103: {
1104: 	const struct machine_desc *mdesc = NULL;
1105: 	void *atags_vaddr = NULL;
1106: 
1107: 	if (__atags_pointer)
1108: 		atags_vaddr = FDT_VIRT_BASE(__atags_pointer);
...
```
上述代码的第1108行中的 __atags_pointer 是 bootloader 通过寄存器 r2 传递给 linux 内核的，它保存的是设备树的起始地址。这里可以理解为 atags_vaddr 是设备树在虚拟地址空间的起始地址。

```c
1102: void __init setup_arch(char **cmdline_p)
1103: {
    ...
1111: 	if (atags_vaddr) {
1112: 		mdesc = setup_machine_fdt(atags_vaddr);
```
接着调用 `setup_machine_fdt()` 函数，该函数的作用就是匹配一个 machine_desc 结构体。一个 linux image 可以启动同一架构的不同 SOC，也可以启动不同架构的 SOC。它是怎么做到的？原来，linux 内核在编译时，会为每个 SOC 生成一个 machine_desc 结构体，该结构体中保存了 SOC 的相关信息，然后根据指定的设备树中的 compatible 字段匹配最合适的 SOC 进行启动。

在代码中定义 SOC 的 machine_desc 结构是使用 `DT_MACHINE_START() 及 MACHINE_END()` 这两个宏。比如源码中有如下定义：

```c
File: arch\arm\mach-imx\mach-imx6ul.c
39: static const char * const imx6ul_dt_compat[] __initconst = {
40: 	"fsl,imx6ul",
41: 	"fsl,imx6ull",
42: 	"fsl,imx6ulz",
43: 	NULL,
44: };
45: 
46: DT_MACHINE_START(IMX6UL, "Freescale i.MX6 Ultralite (Device Tree)")
47: 	.init_irq	= imx6ul_init_irq,
48: 	.init_machine	= imx6ul_init_machine,
49: 	.init_late	= imx6ul_init_late,
50: 	.dt_compat	= imx6ul_dt_compat,
51: MACHINE_END
```

然后设备树中有 compatible 字段，如下：

```
compatible = "fsl,imx6ull-14x14-evk", "fsl,imx6ull";
```
所以，这个设备树会匹配上面的 machine_desc 结构体。最终 `setup_machine_fdt()` 这个函数就返回了上述的 machine_desc 结构体。

此外，在匹配到最佳的 machine_desc 结构后，`setup_machine_fdt()` 函数还会调用 `early_init_dt_scan_nodes()` 函数。该函数的作用:

```
 (1) 扫描/chosen或者/chose@0节点下面的bootargs属性值到boot_command_line，此外，还处理initrd相关的property，并保存在initrd_start和initrd_end这两个全局变量中；

(2) 扫描根节点下面，获取{size,address}-cells信息，并保存在dt_root_size_cells和dt_root_addr_cells全局变量中；

(3) 扫描具有device_type = “memory”属性的/memory或者/memory@0节点下面的reg属性值，并把相关信息保存在meminfo中，全局变量meminfo保存了系统内存相关的信息。 
```

具体的函数调用过程如下图：

![](./images/Snipaste_2024-12-02_22-08-20.png)

# 2.unflatten_device_tree 函数
接下来会调用 `unflatten_device_tree()` 函数，该函数主要就是解析设备树，然后创建 struct device_node 的树状结构。

设备树中的每一个 node 经过 kernel 处理都会生成一个 struct device_node 的结构体，struct device_node 最终一般会被挂接到具体的 struct device 结构体中。struct device_node 的定义如下：

```c
File: include\linux\of.h
48: struct device_node {
49: 	const char *name;   //node的名称，取最后一次"/"和"@"之间子串
50: 	phandle phandle;    //phandle属性值
51: 	const char *full_name;  //指向该结构体结束位置，存放node的路径全名，例如：/chosen
52: 	struct fwnode_handle fwnode;
53: 
54: 	struct	property *properties;   //指向该节点下的第一个属性，其它属性与该属性链表相接
55: 	struct	property *deadprops;	/* removed properties */
56: 	struct	device_node *parent;    //父节点
57: 	struct	device_node *child;     //子节点
58: 	struct	device_node *sibling;   //兄弟节点
59: #if defined(CONFIG_OF_KOBJ)
60: 	struct	kobject kobj;       //sysfs文件系统目录体现
61: #endif
62: 	unsigned long _flags;   //当前node状态标志位，见include/linux/of.h
63: 	void	*data;
64: #if defined(CONFIG_SPARC)
65: 	unsigned int unique_id;
66: 	struct of_irq_controller *irq_trans;
67: #endif
68: };
```

```c
File: drivers\of\fdt.c
1203: /**
1204:  * unflatten_device_tree - create tree of device_nodes from flat blob
1205:  *
1206:  * unflattens the device-tree passed by the firmware, creating the
1207:  * tree of struct device_node. It also fills the "name" and "type"
1208:  * pointers of the nodes so the normal device-tree walking functions
1209:  * can be used.
1210:  */
1211: void __init unflatten_device_tree(void)
1212: {
1213: 	void *fdt = initial_boot_params;
...
1235: 	__unflatten_device_tree(fdt, NULL, &of_root,    //在调用前of_root为NULL，该函数调用后,of_root指向根节点
1236: 				early_init_dt_alloc_memory_arch, false);
1237: 
1238: 	/* Get pointer to "/chosen" and "/aliases" nodes for use everywhere */
1239: 	of_alias_scan(early_init_dt_alloc_memory_arch);
...
1242: }
```

```c
File: drivers\of\fdt.c
351: void *__unflatten_device_tree(const void *blob,
352: 			      struct device_node *dad,
353: 			      struct device_node **mynodes,
354: 			      void *(*dt_alloc)(u64 size, u64 align),
355: 			      bool detached)
356: {
...
380: 
381: 	/* First pass, scan for size */
382: 	size = unflatten_dt_nodes(blob, NULL, dad, NULL);   //第一次调用，只为计算出需要的内存大小
383: 	if (size <= 0)
384: 		return NULL;
385: 
386: 	size = ALIGN(size, 4);
387: 	pr_debug("  size is %d, allocating...\n", size);
388: 
389: 	/* Allocate memory for the expanded device tree */
390: 	mem = dt_alloc(size + 4, __alignof__(struct device_node));  //这里开始分配内存
391: 	if (!mem)
392: 		return NULL;
393: 
394: 	memset(mem, 0, size);
395: 
396: 	*(__be32 *)(mem + size) = cpu_to_be32(0xdeadbeef);
397: 
398: 	pr_debug("  unflattening %p...\n", mem);
399: 
400: 	/* Second pass, do actual unflattening */
401: 	ret = unflatten_dt_nodes(blob, mem, dad, mynodes);  //第二次调用才真正分配内存，并填充数据
...
416: 	return mem;
417: }
```
在unflatten_device_tree()中，调用函数__unflatten_device_tree()，参数initial_boot_params指向Device Tree在内存中的首地址，of_root在经过该函数处理之后，会指向根节点，early_init_dt_alloc_memory_arch是一个函数指针，为struct device_node和struct property结构体分配内存的回调函数（callback）。在__unflatten_device_tree()函数中，两次调用unflatten_dt_node()函数，第一次是为了得到Device Tree转换成struct device_node和struct property结构体需要分配的内存大小，第二次调用才是具体填充每一个struct device_node和struct property结构体。

# 3.第一次调用 unflatten_dt_nodes 函数
接下来看看第一次调用 `unflatten_dt_nodes()` 是怎么仅获取设备树转换成 struct device_node 和 struct property 结构体需要分配的内存大小的。第一次调用时的传参如下：

```c
unflatten_dt_nodes(blob, NULL, dad, NULL);
```

带着这些入参进入该函数看看其内容：

```c
File: drivers\of\fdt.c
270: static int unflatten_dt_nodes(const void *blob,
271: 			      void *mem,
272: 			      struct device_node *dad,
273: 			      struct device_node **nodepp)
274: {
275: 	struct device_node *root;
276: 	int offset = 0, depth = 0, initial_depth = 0;
277: #define FDT_MAX_DEPTH	64
278: 	struct device_node *nps[FDT_MAX_DEPTH];     //节点嵌套深度，不能超过64层
279: 	void *base = mem;
280: 	bool dryrun = !base;    //因为mem=NULL,所以base=NULL,则dryrun=true
281: 	int ret;
282: 
283: 	if (nodepp)     //入参 nodepp为NULL，所以不做处理
284: 		*nodepp = NULL;
285: 
286: 	/*
287: 	 * We're unflattening device sub-tree if @dad is valid. There are
288: 	 * possibly multiple nodes in the first level of depth. We need
289: 	 * set @depth to 1 to make fdt_next_node() happy as it bails
290: 	 * immediately when negative @depth is found. Otherwise, the device
291: 	 * nodes except the first one won't be unflattened successfully.
292: 	 */
293: 	if (dad)        //这里的dad传入的是NULL，所以depth从0开始
294: 		depth = initial_depth = 1;
295: 
296: 	root = dad;
297: 	nps[depth] = dad;   //即 nps[0]=dad，根节点
298: 
        /*
        这里开始循环查找整个Device Tree的所有节点，每个节点的最大嵌套深度不能超过64。
        offset就是每个节点在Device Tree文件中的偏移地址。比如 offset=0 代表是根节点 "/" 的偏移。
        fdt_next_node(blob, offset, &depth) 会返回各个节点在整个Device Tree文件中的偏移地址，并且还会返回当前轮询到的节点的嵌套深度(depth)。
        */
299: 	for (offset = 0;
300: 	     offset >= 0 && depth >= initial_depth;
301: 	     offset = fdt_next_node(blob, offset, &depth)) {
302: 		if (WARN_ON_ONCE(depth >= FDT_MAX_DEPTH - 1))   //嵌套深度限制
303: 			continue;
304: 
305: 		if (!IS_ENABLED(CONFIG_OF_KOBJ) &&
306: 		    !of_fdt_device_is_available(blob, offset))
307: 			continue;
308: 
            /*
            注意入参：mem=NULL,dryrun=true; &mem相当于 &NULL，这不会出问题，相当于取 NULL 的地址。
            该函数会为当前轮询到的节点分配 struce device_node 结构；并且会为当前节点中的所有属性分配 struct property 结构。
            然后把分配内存的大小的值累加到 mem 这个值上。(注意这里其实并没有分配内存，只是为了计算出所需要的内存大小)
            该函数的具体内容见下文分析
            */
309: 		ret = populate_node(blob, offset, &mem, nps[depth],
310: 				   &nps[depth+1], dryrun);
311: 		if (ret < 0)
312: 			return ret;
313: 
            /*
            因为dryrun=true,所以下面的代码不会执行。
            */
314: 		if (!dryrun && nodepp && !*nodepp)
315: 			*nodepp = nps[depth+1];
316: 		if (!dryrun && !root)
317: 			root = nps[depth+1];
318: 	}
319: 
320: 	if (offset < 0 && offset != -FDT_ERR_NOTFOUND) {
321: 		pr_err("Error %d processing FDT\n", offset);
322: 		return -EINVAL;
323: 	}
324: 
325: 	/*
326: 	 * Reverse the child list. Some drivers assumes node order matches .dts
327: 	 * node order
328: 	 */
329: 	if (!dryrun)
330: 		reverse_nodes(root);    //该代码不会执行
331: 
        /*
        经过前面的for循环，此时已遍历了Device Tree的所有节点，并且计算出所需要的 struct device_node 和 struct property 结构体所占的内存大小。(注意，并没有分配内存)
        该大小全都累加到 mem 这个值上了；
        我们知道，第一次调用该函数时，mem=NULL=0, base也被赋值为NULL，假如整个Device Tree中所有的 struct device_node 和 struct property 结构体所占内存大小为 1000 字节，那么 mem=1000。
        所以 mem-base=1000，
        即该函数返回值就是整个Device Tree转换成 struct device_node 和 struct property 结构体所占的内存大小。
        */
332: 	return mem - base;  
333: }
```

# 4.分配 node 内存并初始化
接下看一下 `populate_node()` 函数。

```c
File: drivers\of\fdt.c
191: static int populate_node(const void *blob,
192: 			  int offset,
193: 			  void **mem,
194: 			  struct device_node *dad,
195: 			  struct device_node **pnp,
196: 			  bool dryrun)
197: {
198: 	struct device_node *np;
199: 	const char *pathp;
200: 	int len;
201: 
        /*
        先介绍一下该函数的入参：blob为设备树的起始地址；offset为当前轮询到的节点在Device Tree文件中的偏移地址；len为当前节点的名字的长度；
        该函数功能为获取指定node的名字，len为该node的名字的长度，该函数返回该名字的起始地址；
        例如有这样一个node: "/serial@e2900800"，则该函数的返回值为 "/serial@e2900800"; 
        它返回node名字全路径的首地址。
        （注意，该函数中会判断当前Device Tree的版本号，如果版本号小于0x10则node的名字将不是全路径，但目前使用的设备树版本都大于0x10了）
        */
202: 	pathp = fdt_get_name(blob, offset, &len);
203: 	if (!pathp) {
204: 		*pnp = NULL;
205: 		return len;
206: 	}
207: 
208: 	len++;  // len++为预留一个结束符'\0'的位置
209: 
        /*
        为当前node为预留一个 struct device_node 结构空间，另外再多预留一个 len空间，即node名字。
        */
210: 	np = unflatten_dt_alloc(mem, sizeof(struct device_node) + len,
211: 				__alignof__(struct device_node));
        /*
        经过前面分析，第一次调用时的dryrun=true;目的只为得到待分配内存的大小，实际并没有分配内存
        第二次调用时dryrun=false;才会为分配实际内存并为结构成员进行赋值
        */
212: 	if (!dryrun) {
213: 		char *fn;
214: 		of_node_init(np);   //初始化np->kobj结构
            /*
            上面第210行预约空间时，node的全路径空间是在 struct device_node 结构之后，
            所以这里 np->full_name 指向的空间需要加上 sizeof(*np) 的偏移量。
            */
215: 		np->full_name = fn = ((char *)np) + sizeof(*np);
216: 
217: 		memcpy(fn, pathp, len); //这里是把node的全路径名字赋值给np->full_name
218: 
            /*
            当前节点挂接到相应的父节点，子节点，兄弟节点
            */
219: 		if (dad != NULL) {
220: 			np->parent = dad;
221: 			np->sibling = dad->child;
222: 			dad->child = np;
223: 		}
224: 	}
225: 
        /*
        处理当前节点下的所有属性；该函数内存见下文分析
        该函数执行完成后，会在当前Node节点中添加一个 "name" 的属性，其值就是当前节点的名字。
        */
226: 	populate_properties(blob, offset, mem, np, pathp, dryrun);
227: 	if (!dryrun) {
            /*
            第二次调用会执行下面代码：
            由于第226行中会在当前Node中添加"name"这个属性，所以下面函数就是获取这个 "name"属性的值，即当前Node的名字。
            然后赋值给 np->name
            */
228: 		np->name = of_get_property(np, "name", NULL);
229: 		if (!np->name)
230: 			np->name = "<NULL>";
231: 	}
232: 
233: 	*pnp = np;
234: 	return 0;
235: }
```

# 5.分配 property 内存并初始化
接下看一下 `populate_properties()` 函数。

```c
File: drivers\of\fdt.c
094: static void populate_properties(const void *blob,
095: 				int offset,
096: 				void **mem,
097: 				struct device_node *np,
098: 				const char *nodename,
099: 				bool dryrun)
100: {
101: 	struct property *pp, **pprev = NULL;
102: 	int cur;
103: 	bool has_name = false;
104: 
105: 	pprev = &np->properties;
        /*
        遍历当前节点的所有property
        */
106: 	for (cur = fdt_first_property_offset(blob, offset);
107: 	     cur >= 0;
108: 	     cur = fdt_next_property_offset(blob, cur)) {
109: 		const __be32 *val;
110: 		const char *pname;
111: 		u32 sz;
112: 
            /*
            该函数的返回值 val 为 property 的值，pname 为 property 的名字(键)，sz 为 property 的长度。
            */
113: 		val = fdt_getprop_by_offset(blob, cur, &pname, &sz);
114: 		if (!val) {
115: 			pr_warn("Cannot locate property at 0x%x\n", cur);
116: 			continue;
117: 		}
118: 
119: 		if (!pname) {
120: 			pr_warn("Cannot find property name at 0x%x\n", cur);
121: 			continue;
122: 		}
123: 
            /*
            如果当前property的名字为 "name",则把has_name标记为true，后面会判断该值
            */
124: 		if (!strcmp(pname, "name"))
125: 			has_name = true;
126: 
            /*
            为当前property预留内存空间；
            如果是第一次调用，则该函数只是计算一下 struct property 结构的大小；并不是分配内存空间
            如果是第二次调用，则是真正的要给struct property 分配内存
            */
127: 		pp = unflatten_dt_alloc(mem, sizeof(struct property),
128: 					__alignof__(struct property));
            /*
            第一次调用时，dryrun=true，所以不会继续向下走，循环获得该Node下所有property转换成struct property结构所需要内存的大小
            */
129: 		if (dryrun)
130: 			continue;
131: 
132: 		/* We accept flattened tree phandles either in
133: 		 * ePAPR-style "phandle" properties, or the
134: 		 * legacy "linux,phandle" properties.  If both
135: 		 * appear and have different values, things
136: 		 * will get weird. Don't do that.
137: 		 */
            /*
            当设备树中存在Node引用时，在编译设备树的时候，DTC会自动为被引用的节点中添加"phandle"，"linux,phandle"属性，
            */
138: 		if (!strcmp(pname, "phandle") ||
139: 		    !strcmp(pname, "linux,phandle")) {
140: 			if (!np->phandle)
141: 				np->phandle = be32_to_cpup(val);
142: 		}
143: 
144: 		/* And we process the "ibm,phandle" property
145: 		 * used in pSeries dynamic device tree
146: 		 * stuff
147: 		 */
148: 		if (!strcmp(pname, "ibm,phandle"))
149: 			np->phandle = be32_to_cpup(val);
150: 
            /*
            初始化 struct property 结构成员
            */
151: 		pp->name   = (char *)pname;
152: 		pp->length = sz;
153: 		pp->value  = (__be32 *)val;
154: 		*pprev     = pp;
155: 		pprev      = &pp->next; //同一个Node下的所有property形成单向链表
156: 	}
157: 
158: 	/* With version 0x10 we may not have the name property,
159: 	 * recreate it here from the unit name if absent
160: 	 */
        /*
        如果当前Node下的所有property中没有name属性，则根据Node的全路径自己获取一个name值；
        由此可知，每个Node下面都会存在一个 "name" 的属性(如果没有，则下面的代码会自动给生成一个)
        比如当前Node的全路径为 "/soc/aips-bus@02000000/spba-bus@02000000/ecspi@02010000",
        则经过下面的代码，pp->value = "ecspi@02010000"
        */
161: 	if (!has_name) {
162: 		const char *p = nodename, *ps = p, *pa = NULL;
163: 		int len;
164: 
165: 		while (*p) {
166: 			if ((*p) == '@')
167: 				pa = p;
168: 			else if ((*p) == '/')
169: 				ps = p + 1;
170: 			p++;
171: 		}
172: 
173: 		if (pa < ps)
174: 			pa = p;
175: 		len = (pa - ps) + 1;
176: 		pp = unflatten_dt_alloc(mem, sizeof(struct property) + len,
177: 					__alignof__(struct property));
            /*
            第二次调用时会初始化property结构的各成员变量
            */
178: 		if (!dryrun) {
179: 			pp->name   = "name";
180: 			pp->length = len;
181: 			pp->value  = pp + 1;
182: 			*pprev     = pp;
183: 			memcpy(pp->value, ps, len - 1);
184: 			((char *)pp->value)[len - 1] = 0;
185: 			pr_debug("fixed up name for %s -> %s\n",
186: 				 nodename, (char *)pp->value);
187: 		}
188: 	}
189: }
```

# 6.第二次调用 unflatten_dt_nodes 函数
接下来看一下第二次调用 `unflatten_dt_nodes()` 要完成什么样的工作：

```c
File: drivers\of\fdt.c
270: static int unflatten_dt_nodes(const void *blob,
271: 			      void *mem,
272: 			      struct device_node *dad,
273: 			      struct device_node **nodepp)
274: {
275: 	struct device_node *root;
276: 	int offset = 0, depth = 0, initial_depth = 0;
277: #define FDT_MAX_DEPTH	64
278: 	struct device_node *nps[FDT_MAX_DEPTH];
279: 	void *base = mem;       //第二次调用时，mem已经指向了分配好的内存起始地址了
280: 	bool dryrun = !base;    //dryrun=false
281: 	int ret;
282: 
283: 	if (nodepp)
284: 		*nodepp = NULL;
285: 
286: 	/*
287: 	 * We're unflattening device sub-tree if @dad is valid. There are
288: 	 * possibly multiple nodes in the first level of depth. We need
289: 	 * set @depth to 1 to make fdt_next_node() happy as it bails
290: 	 * immediately when negative @depth is found. Otherwise, the device
291: 	 * nodes except the first one won't be unflattened successfully.
292: 	 */
293: 	if (dad)    //dad依旧是NULL，所以depth还是从0开始
294: 		depth = initial_depth = 1;
295: 
296: 	root = dad;
297: 	nps[depth] = dad;   //nps[0] = dad
298: 
        //循环遍历所有Node
299: 	for (offset = 0;
300: 	     offset >= 0 && depth >= initial_depth;
301: 	     offset = fdt_next_node(blob, offset, &depth)) {
302: 		if (WARN_ON_ONCE(depth >= FDT_MAX_DEPTH - 1))
303: 			continue;
304: 
305: 		if (!IS_ENABLED(CONFIG_OF_KOBJ) &&
306: 		    !of_fdt_device_is_available(blob, offset))
307: 			continue;
308:   
            /*
            该函数前面分析过，分配当前Node节点，并分配当前Node下的所有property，并分配Node的name空间，
            并且对struct device_node 结构体成员进行初始化，对struct property 结构体成员进行初始化。
            另外，在该函数中初始化完成了当前Node的struct device_node 结构体后，返回给该函数的第5个参数，即nps[depth+1]
            */
309: 		ret = populate_node(blob, offset, &mem, nps[depth],
310: 				   &nps[depth+1], dryrun);
311: 		if (ret < 0)
312: 			return ret;
313: 
314: 		if (!dryrun && nodepp && !*nodepp)
315: 			*nodepp = nps[depth+1];
316: 		if (!dryrun && !root)
317: 			root = nps[depth+1];    //这里会把第一个Node保存在root中，即根节点，最终会传出该函数，在外层对应 of_root 这个值。
318: 	}
319: 
320: 	if (offset < 0 && offset != -FDT_ERR_NOTFOUND) {
321: 		pr_err("Error %d processing FDT\n", offset);
322: 		return -EINVAL;
323: 	}
324: 
325: 	/*
326: 	 * Reverse the child list. Some drivers assumes node order matches .dts
327: 	 * node order
328: 	 */
329: 	if (!dryrun)
330: 		reverse_nodes(root);    //反转以 root 为根节点的设备树节点顺序。
331: 
332: 	return mem - base;
333: }
```

# 7.总结
最后画图总结一下：

![](./draw/kernel的设备树解析流程.png)
