本文分析基于内核版本 6.12.0

[TOC]

# 1.platform 模块的初始化
有关 platform 相关的操作代码路径如下：

![](./images/Snipaste_2024-12-06_23-07-18.png)

接下来看一下 `platform_bus_init` 函数的实现：

```c
File: drivers\base\platform.c
1512: int __init platform_bus_init(void)
1513: {
1514: 	int error;
1515: 
1516: 	early_platform_cleanup();
1517: 
1518: 	error = device_register(&platform_bus);
1519: 	if (error) {
1520: 		put_device(&platform_bus);
1521: 		return error;
1522: 	}
1523: 	error =  bus_register(&platform_bus_type);
1524: 	if (error)
1525: 		device_unregister(&platform_bus);
1526: 
1527: 	return error;
1528: }
```

第1516行，清除所有和 early device/driver 相关的代码。内核启动时要完成一定的初始化操作之后才会处理device和driver的注册及probe，因此在这之前，常规的platform设备是无法使用的。但是Linux中，有些设备需要尽早使用(如在启动过程中充当console输出的serial设备)，所以platform模块提供了一种称作early platform device/driver的机制，允许驱动开发人员在开发驱动时，向内核注册可在内核早期启动过程中使用的driver。

代码运行到第1516行时说明系统已经完成了early阶段的启动，转而进行正常的设备初始化、启动操作。所以不再需要early platform相关的东西了。

第1518行，注册platform_bus这个platform device。platform_bus 定义很简单，只包含了 init_name 成员，如下：

```c
File: drivers\base\platform.c
42: struct device platform_bus = {
43: 	.init_name	= "platform",
44: };
```

该步骤会在sysfs中创建`"/sys/devices/platform/"`目录，所有的Platform设备，都会包含在此目录下。

第1523行调用`bus_register()`注册一个名称为`platform_bus_type`的bus，其定义如下：

```c
File: drivers\base\platform.c
1477: const struct bus_type platform_bus_type = {
1478: 	.name		= "platform",
1479: 	.dev_groups	= platform_dev_groups,
1480: 	.match		= platform_match,
1481: 	.uevent		= platform_uevent,
1482: 	.probe		= platform_probe,
1483: 	.remove		= platform_remove,
1484: 	.shutdown	= platform_shutdown,
1485: 	.dma_configure	= platform_dma_configure,
1486: 	.dma_cleanup	= platform_dma_cleanup,
1487: 	.pm		= &platform_dev_pm_ops,
1488: };
```

该步骤会在sysfs中创建`"/sys/bus/platform/"`目录，同时，会在`"/sys/bus/platform/"`目录下，创建uevent attribute（/sys/bus/platform/uevent）、devices目录、drivers目录、drivers_probe和drivers_autoprobe两个attribute（/sys/bus/platform/drivers_probe 和 /sys/bus/platform/drivers_autoprobe）。

# 2.machine 初始化
回到`do_basic_setup()`函数，在该函数中会调用`do_initcalls()`函数。do_initcalls 函数会依次执行各个initcall函数。do_initcalls 函数定义如下：

```c
File: init\main.c
1334: static void __init do_initcalls(void)
1335: {
1336: 	int level;
...
1344: 	for (level = 0; level < ARRAY_SIZE(initcall_levels) - 1; level++) {
1345: 		/* Parser modifies command_line, restore it each time */
1346: 		strcpy(command_line, saved_command_line);
1347: 		do_initcall_level(level, command_line);
1348: 	}
...
1351: }
```
该函数中的for循环的次数取决于`initcall_levels`数组的长度，该数组定义如下：

```c
File: init\main.c
1289: static initcall_entry_t *initcall_levels[] __initdata = {
1290: 	__initcall0_start,
1291: 	__initcall1_start,
1292: 	__initcall2_start,
1293: 	__initcall3_start,
1294: 	__initcall4_start,
1295: 	__initcall5_start,
1296: 	__initcall6_start,
1297: 	__initcall7_start,
1298: 	__initcall_end,
1299: };
```

initcall_levels数组中的成员的地址由链接脚本指定，链接脚本有如下内容：

```
        __initcall0_start = .; 
        KEEP(*(.initcall0.init)) 
        KEEP(*(.initcall0s.init)) 
        __initcall1_start = .; 
        KEEP(*(.initcall1.init)) 
        KEEP(*(.initcall1s.init)) 
        __initcall2_start = .; 
        KEEP(*(.initcall2.init)) 
        KEEP(*(.initcall2s.init)) 
        __initcall3_start = .; 
        KEEP(*(.initcall3.init)) 
        KEEP(*(.initcall3s.init)) 
        __initcall4_start = .; 
        KEEP(*(.initcall4.init)) 
        KEEP(*(.initcall4s.init)) 
        __initcall5_start = .; 
        KEEP(*(.initcall5.init)) 
        KEEP(*(.initcall5s.init)) 
        __initcallrootfs_start = .; 
        KEEP(*(.initcallrootfs.init)) 
        KEEP(*(.initcallrootfss.init)) 
        __initcall6_start = .; 
        KEEP(*(.initcall6.init)) 
        KEEP(*(.initcall6s.init)) 
        __initcall7_start = .; 
        KEEP(*(.initcall7.init)) 
        KEEP(*(.initcall7s.init)) 
        __initcall_end = .; 
```

目前来看，kernel的初始化initcall定义了7类。那代码中是怎么定义initcall函数并存放到对应的段中呢？其实还是老方法，使用一个宏定义，内容如下：

```c
File: include\linux\init.h
298: #define pure_initcall(fn)		__define_initcall(fn, 0)
299: 
300: #define core_initcall(fn)		__define_initcall(fn, 1)
301: #define core_initcall_sync(fn)		__define_initcall(fn, 1s)
302: #define postcore_initcall(fn)		__define_initcall(fn, 2)
303: #define postcore_initcall_sync(fn)	__define_initcall(fn, 2s)
304: #define arch_initcall(fn)		__define_initcall(fn, 3)
305: #define arch_initcall_sync(fn)		__define_initcall(fn, 3s)
306: #define subsys_initcall(fn)		__define_initcall(fn, 4)
307: #define subsys_initcall_sync(fn)	__define_initcall(fn, 4s)
308: #define fs_initcall(fn)			__define_initcall(fn, 5)
309: #define fs_initcall_sync(fn)		__define_initcall(fn, 5s)
310: #define rootfs_initcall(fn)		__define_initcall(fn, rootfs)
311: #define device_initcall(fn)		__define_initcall(fn, 6)
312: #define device_initcall_sync(fn)	__define_initcall(fn, 6s)
313: #define late_initcall(fn)		__define_initcall(fn, 7)
314: #define late_initcall_sync(fn)		__define_initcall(fn, 7s)
```

逐层展开宏定义可理解上述宏，这里直接给出结论，比如:

```c
device_initcall(fn)
//展开后的结果：
static initcall_t __initcall__modname__counter_line_fn6 __used __attribute__((__section__(.initcall6.init))) = fn;
//上述生成的变量名中的 modname, counter, line 值会变化
```

好了，回到`do_initcalls()`函数中，经过上面的分析可知，该函数中会循环调用 7 次 `do_initcall_level()`函数，每次调用都会调用一个段中的所有initcall函数。接下来看一下`do_initcall_level()`函数。

```c
File: init\main.c
1319: static void __init do_initcall_level(int level, char *command_line)
1320: {
1321: 	initcall_entry_t *fn;
...
1330: 	for (fn = initcall_levels[level]; fn < initcall_levels[level+1]; fn++)
1331: 		do_one_initcall(initcall_from_entry(fn));
1332: }
```
可以看到，在`do_initcall_level()`函数中会该段(level)下的所有使用`__define_initcall`宏定义的函数，依次调用。而 `do_one_initcall()` 函数则真正执行 fn 函数。

所以，经过两个大循环后，会把系统中属于 `.initcall0.init/.initcall0s.init` 到 `.initcall7.init/.initcall7s.init` 这 7 个段中的所有函数都调用了。

machine 的初始化函数是在 .initcall3.init 的段中，即代码中会使用 `arch_initcall()`，如下：

```c
File: arch\arm\kernel\setup.c
942: static int __init customize_machine(void)
943: {
944: 	/*
945: 	 * customizes platform devices, or adds new ones
946: 	 * On DT based machines, we fall back to populating the
947: 	 * machine from the device tree, if no callback is provided,
948: 	 * otherwise we would always need an init_machine callback.
949: 	 */
950: 	if (machine_desc->init_machine)
951: 		machine_desc->init_machine();
952: 
953: 	return 0;
954: }
955: arch_initcall(customize_machine);
```

同此可见，在`do_initcalls()`函数的执行过程中会调用 `customize_machine()`函数，从上面的代码中知道，该函数调用了 machine_desc 中的 init_machine 成员函数。而 machine_desc 在《kernel设备树解析流程.md》一文中有讲到，匹配到的 machine desc 如下：

```c
File: arch\arm\mach-imx\mach-imx6ul.c
39: static const char * const imx6ul_dt_compat[] __initconst = {
40: 	"fsl,imx6ul",
41: 	"fsl,imx6ull",
42: 	"fsl,imx6ulz",
43: 	NULL,
44: };
45: 
46: DT_MACHINE_START(IMX6UL, "Freescale i.MX6 Ultralite (Device Tree)")
47: 	.init_irq	= imx6ul_init_irq,
48: 	.init_machine	= imx6ul_init_machine,
49: 	.init_late	= imx6ul_init_late,
50: 	.dt_compat	= imx6ul_dt_compat,
51: MACHINE_END
```

所以 machine_desc->init_machine()，其实就是调用了 `imx6ul_init_machine` 函数。

# 3.platform device 的创建
通过前面的分析，最终会调用 `imx6ul_init_machine` 函数。接下来就看一下该函数：

```c
File: arch\arm\mach-imx\mach-imx6ul.c
13: static void __init imx6ul_init_machine(void)
14: {
15: 	imx_print_silicon_rev(cpu_is_imx6ull() ? "i.MX6ULL" : "i.MX6UL",
16: 		imx_get_soc_revision());
17: 
        //一个入参为NULL表示从设备树的根开始扫描
18: 	of_platform_default_populate(NULL, NULL, NULL);
19: 	imx_anatop_init();
20: 	imx6ul_pm_init();
21: }
22: 
```
重点关注一下第18行。`of_platform_default_populate` 函数定义如下：

```c
File: drivers\of\platform.c
483: int of_platform_default_populate(struct device_node *root,
484: 				 const struct of_dev_auxdata *lookup,
485: 				 struct device *parent)
486: {
487: 	return of_platform_populate(root, of_default_bus_match_table, lookup,
488: 				    parent);
489: }
```

其中的 `of_default_bus_match_table` 匹配表定义如下：
```c
File: drivers\of\platform.c
27: const struct of_device_id of_default_bus_match_table[] = {
28: 	{ .compatible = "simple-bus", },
29: 	{ .compatible = "simple-mfd", },
30: 	{ .compatible = "isa", },
31: #ifdef CONFIG_ARM_AMBA
32: 	{ .compatible = "arm,amba-bus", },
33: #endif /* CONFIG_ARM_AMBA */
34: 	{} /* Empty terminated list */
35: };
```

`of_platform_populate()` 函数的主要作用就是遍历根节点下面的所有子节点，如果该节点符合条件(至于需要符合什么条件，则在 of_platform_bus_create 函数中有限制)，则为该子节点创建 struct platform_device 结构。具体函数内容如下：

```c
File: drivers\of\platform.c
454: int of_platform_populate(struct device_node *root,
455: 			const struct of_device_id *matches,
456: 			const struct of_dev_auxdata *lookup,
457: 			struct device *parent)
458: {
459: 	int rc = 0;
460: 
460: 	// 获取根节点
461: 	root = root ? of_node_get(root) : of_find_node_by_path("/");
462: 	if (!root)
463: 		return -EINVAL;
464: 
465: 	pr_debug("%s()\n", __func__);
466: 	pr_debug(" starting at: %pOF\n", root);
467: 
468: 	device_links_supplier_sync_state_pause();
        /*
        从根节点开始遍历其所有子节点，
        child是在该宏内定义成一个 struct device_node 指定变量，迭代每个子节点
        */
469: 	for_each_child_of_node_scoped(root, child) {
470: 		rc = of_platform_bus_create(child, matches, lookup, parent, true);
471: 		if (rc)
472: 			break;
473: 	}
474: 	device_links_supplier_sync_state_resume();
475: 
        //更新device_node flag标志位
476: 	of_node_set_flag(root, OF_POPULATED_BUS);
477: 
478: 	of_node_put(root);
479: 	return rc;
480: }
```
从`of_platform_populate()`函数可以看出，每获取一个子节点都会调用 `of_platform_bus_create()`函数，其定义如下：

```c
File: drivers\of\platform.c
            /*
            bus：要创建的那个device_node
            matches:要匹配的list (这是入参为NULL)
            lookup:附属数据     (这是入参为NULL)
            parent:父节点       (这是入参为NULL)
            strict:是否要求完全匹配     (这是入参为true)
            */
335: static int of_platform_bus_create(struct device_node *bus,
336: 				  const struct of_device_id *matches,
337: 				  const struct of_dev_auxdata *lookup,
338: 				  struct device *parent, bool strict)
339: {
340: 	const struct of_dev_auxdata *auxdata;
341: 	struct platform_device *dev;
342: 	const char *bus_id = NULL;
343: 	void *platform_data = NULL;
344: 	int rc = 0;
345: 
346: 	/* Make sure it has a compatible property */
        /*
        从这里可以看出，创建platform_device的前提是该节点必须拥有 compatible 属性.
        */
347: 	if (strict && (!of_get_property(bus, "compatible", NULL))) {
348: 		pr_debug("%s() - skipping %pOF, no compatible prop\n",
349: 			 __func__, bus);
350: 		return 0;
351: 	}
352: 
353: 	/* Skip nodes for which we don't want to create devices */
        //跳过不需要创建platfomr_device的节点，其中不需要创建的节点的compatible表在of_skipped_node_table中定义
354: 	if (unlikely(of_match_node(of_skipped_node_table, bus))) {
355: 		pr_debug("%s() - skipping %pOF node\n", __func__, bus);
356: 		return 0;
357: 	}
358: 
        //跳过已经创建过platform_device的节点
359: 	if (of_node_check_flag(bus, OF_POPULATED_BUS)) {
360: 		pr_debug("%s() - skipping %pOF, already populated\n",
361: 			__func__, bus);
362: 		return 0;
363: 	}
364: 
365: 	auxdata = of_dev_lookup(lookup, bus);
366: 	if (auxdata) {
367: 		bus_id = auxdata->name;
368: 		platform_data = auxdata->platform_data;
369: 	}
370: 
        /*
        ARM公司提供了cpu core，除此之外，它设计了AMBA总线来连接SOC内部的各个block。符合这个总线标准
        的SOC上的外设叫做ARM Primecell Peripheral。如果一个device node的compatible属性值是 arm,primecell,
        则调用 of_amba_device_create 函数来向amba总线上增加一个amba device
        */
371: 	if (of_device_is_compatible(bus, "arm,primecell")) {
372: 		/*
373: 		 * Don't return an error here to keep compatibility with older
374: 		 * device tree files.
375: 		 */
376: 		of_amba_device_create(bus, bus_id, platform_data, parent);
377: 		return 0;
378: 	}
379: 
        //如果不是ARM Primecell Peripheral，则向platform bus上增加一个platform device
380: 	dev = of_platform_device_create_pdata(bus, bus_id, platform_data, parent);
        //如果获取到的device_node(即入参bus)与matches匹配，则说明该device_node本身还是一条总线，所以
        //本函数不能返回，继续执行384行的for循环进行递归匹配查找。
381: 	if (!dev || !of_match_node(matches, bus))
382: 		return 0;
383: 
        /*
        如果该device_node的compatible的属性值为"simple-bus","simple-mfd","isa","arm,amba-bus"
        其中的任一个，则进行递归解析
        */
384: 	for_each_child_of_node_scoped(bus, child) {
385: 		pr_debug("   create child: %pOF\n", child);
386: 		rc = of_platform_bus_create(child, matches, lookup, &dev->dev, strict);
387: 		if (rc)
388: 			break;
389: 	}
390: 	of_node_set_flag(bus, OF_POPULATED_BUS);
391: 	return rc;
392: }
```
上面函数的重点是调用了 `of_platform_device_create_pdata()`函数，看一下该函数内容：

```c
File: drivers\of\platform.c
161: static struct platform_device *of_platform_device_create_pdata(
162: 					struct device_node *np,
163: 					const char *bus_id,
164: 					void *platform_data,
165: 					struct device *parent)
166: {
167: 	struct platform_device *dev;
168: 
169: 	pr_debug("create platform device: %pOF\n", np);
170: 
171: 	if (!of_device_is_available(np) ||
172: 	    of_node_test_and_set_flag(np, OF_POPULATED))
173: 		return NULL;
174: 
        /*
        of_device_alloc()除了分配struct platform_device结构的内存，还分配了该platform_device需要的
        resource的内存(即struct platform_device中的resource成员)。当然，这就需要解析该device node的
        interrupt资源以及memory address资源
        */
175: 	dev = of_device_alloc(np, bus_id, parent);
176: 	if (!dev)
177: 		goto err_clear_flag;
178: 
        //设置platform_device中的其它成员
179: 	dev->dev.coherent_dma_mask = DMA_BIT_MASK(32);
180: 	if (!dev->dev.dma_mask)
181: 		dev->dev.dma_mask = &dev->dev.coherent_dma_mask;
182: 	dev->dev.bus = &platform_bus_type;
183: 	dev->dev.platform_data = platform_data;
184: 	of_msi_configure(&dev->dev, dev->dev.of_node);
185: 
        //把这个platform_device加入到统一设备模型系统中
186: 	if (of_device_add(dev) != 0) {
187: 		platform_device_put(dev);
188: 		goto err_clear_flag;
189: 	}
190: 
191: 	return dev;
192: 
193: err_clear_flag:
194: 	of_node_clear_flag(np, OF_POPULATED);
195: 	return NULL;
196: }
```

`of_device_alloc()` 函数分配到一个 struct platfomr_device 结构体会顺带初始化其中的部分成员变量，其中 `->pdev.dev.release = platform_device_release` 用于释放该结构体占用的资源的回调函数。然后获取资源个数，为每个资源分配一个 `struct resource`结构体并进行成员内容的填充。如果入参parent为空，则platform_device的dev.parent成员将会被赋值为 `platform_bus`，该值在第1节"platform模块的初始化"中有定义；如果入参parent不为空，则platform_device的dev.parent成员将会被赋值为入参parent。

这样的话，通过最外层的 for 循环与里层的递归，最终可以把设备树的所有总线下的所有节点都转换成一个个 platform_device，然后把它们加入到统一设备模型中。更直观的函数调用流程图可见第5小节。

# 4.i2c_client和device_node绑定
经过前面的分析，当`customize_machine()`函数执行完成后，设备树中符合条件的node都已转换成了platform_device，并加入到了统一设备模型中了。当然这里面就包含i2c adapter设备(其驱动是SOC厂商实现)。为什么这样说，我们来看一下设备树文件关于i2c外设如下：

```
        aips2: bus@2100000 {
                compatible = "fsl,aips-bus", "simple-bus";
                #address-cells = <1>;
                #size-cells = <1>;
                reg = <0x02100000 0x100000>;
                ranges;

                ...

                i2c1: i2c@21a0000 {
                        #address-cells = <1>;
                        #size-cells = <0>;
                        compatible = "fsl,imx6ul-i2c", "fsl,imx21-i2c";
                        reg = <0x021a0000 0x4000>;
                        interrupts = <GIC_SPI 36 IRQ_TYPE_LEVEL_HIGH>;
                        clocks = <&clks IMX6UL_CLK_I2C1>;
                        status = "disabled";
                };
                ...
        };
```
从上面的设备树片段可以了解到，i2c1这个外设是aips2的子节点，而aips2存在compatible属性，且其中有一个值为"simple-bus"。经过前面第3小节的分析，遇到compatible属性值为"simple-bus"的节点将会递归解析其子节点，所以会解析到i2c1这个节点，而i2c1这个节点中也存在compatible属性，所以代码会给i2c1这个节点创建一个platform_deivce结构。

所以我们全局搜索一下"fsl,imx6ul-i2c", "fsl,imx21-i2c"字符串，可以在源码中找到其SOC厂商为其实现的驱动。如下：

```c
File: drivers\i2c\busses\i2c-imx.c
277: static const struct of_device_id i2c_imx_dt_ids[] = {
...
284: 	{ .compatible = "fsl,imx6ul-i2c", .data = &imx6_i2c_hwdata, },
...
291: 	{ /* sentinel */ }
292: };
```

`i2c_imx_dt_ids`该数组的地址会被赋值给 platform_driver 结构中成员 driver 的 of_match_table。而 `platform_driver`结构会利用使用 `platform_driver_register()`函数注册到统一设备模型中。代码如下：

```c
File: drivers\i2c\busses\i2c-imx.c
1577: static struct platform_driver i2c_imx_driver = {
1578: 	.probe = i2c_imx_probe,
1579: 	.remove_new = i2c_imx_remove,
1580: 	.driver = {
1581: 		.name = DRIVER_NAME,
1582: 		.pm = pm_ptr(&i2c_imx_pm_ops),
1583: 		.of_match_table = i2c_imx_dt_ids,
1584: 		.acpi_match_table = i2c_imx_acpi_ids,
1585: 	},
1586: 	.id_table = imx_i2c_devtype,
1587: };
1588: 
1589: static int __init i2c_adap_imx_init(void)
1590: {
1591: 	return platform_driver_register(&i2c_imx_driver);
1592: }
1593: subsys_initcall(i2c_adap_imx_init);
1594: 
1595: static void __exit i2c_adap_imx_exit(void)
1596: {
1597: 	platform_driver_unregister(&i2c_imx_driver);
1598: }
1599: module_exit(i2c_adap_imx_exit);
```
当`i2c_adap_imx_init()`函数被执行时，会向设备模型中注册一个platform_driver的驱动，该驱动一旦被注册，则platform_driver结构的成员.probe指向的函数将被调用(这是设备模型执行的，关于设备模型在其它文章中分析)。但是 `i2c_adap_imx_init()` 会在什么时候被调用勒？看一下上述代码中的第1593行使用了 `subsys_initcall()`宏。还记得上文第2小节提到的这个宏吗？该宏会将`i2c_adap_imx_init()`函数放到`.initcall4.init`的段中，这样的话内核启动时，在执行`do_initcalls()`函数会是轮询到该段，所以`i2c_adap_imx_init()`函数就会被执行到，进而 i2c_imx_driver.probe 也会被调用，即`i2c_imx_probe`函数。

`i2c_imx_probe`就不分析了，直接见第5小节的流程图。


# 5.函数调用流程总结
![](./draw/platform设备绑定过程.png)

上图对应的文件名为 "platform设备绑定过程.png"。