[TOC]

# 1. 7路PLL时钟源
本章内容参考芯片手册的第10章(Chapter 10 Clock and Power Management)和第18章(Chapter 18 Clock Controller Module(CCM))。

板载的外部时钟输入源为 24 MHZ 的晶振以及 32.768KHZ 晶振给 RTC 提供时钟源。

I.MX6ULL的外设有很多，不同的外设时钟源不同，NXP将这些外设的时钟源进行了分组，一共有7组，这7组时钟源都是从 24MHZ 晶振 PLL 而来的，因此也叫做 7组 PLL。这7组PLL如下图：

![](./images/Snipaste_2024-10-31_22-04-36.png)


- ARM_PLL(PLL1)：此路PLL是供ARM内核使用的，ARM内核时钟就是由此PLL生成的，此PLL通过编程的方式最高可倍频到1.3GHz。
- 528_PLL(PLL2)：此路PLL也叫做System_PLL，此路PLL是固定的22倍频，不可编程修改。因此，此路PLL时钟=24MHZ*22=528MHZ。这也是为什么此PLL叫做528_PLL的原因。此PLL分出了4路PFD，分别为：PLL2_PFD0、PLL2_PFD1、PLL2_PFD2、PLL2_PFD3，这4路PFD和528_PLL共同作为其它很多外设的根时钟源。通过528_PLL和这4路PFD是I.MX6ULL内部系统总线的时钟源，比如内部处理逻辑单元、DDR接口、NAND/NOR接口等等。
- USB1_PLL(PLL3)：此路PLL主要用于USBPHY，此PLL也有四路PFD，为PLL3_PFD0、PLL3_PFD1、PLL3_PFD2、PLL3_PFD3。USB1_PLL是固定的20倍频，因此USB1_PLL=24MHZ*20=480MHZ。USB1_PLL虽然主要用于USB1PHY，但是其和四路PDF同样也可以作为其他外设的根时钟源。
- USB2_PLL(PLL7)：没有写错，它在芯片手册上确实是PLL7。看名字就知道此路PLL是给USB2PHY使用的。同样的，此路PLL固定为20倍频。
- ENET_PLL(PLL6)：此路PLL固定为(20+5/6)倍频，因此ENET_PLL=24MHZ*(20+5/6)=500MHZ。此路PLL用于生成网络所需的时钟，可以在此PLL的基础上生成25、50、100、125MHZ的网络时钟。
- VIDEO_PLL(PLL5)：此路PLL用于显示相关的外设，比如LCD。此路PLL的倍频可以调整，PLL的输出范围在650MHZ到1300MHZ。此路PLL在最终输出的时候还可以进行分频，可选1、2、4、8、16分频。
- AUDIO_PLL(PLL4)：此路PLL用于音频相关的外设。此路PLL的倍频可以调整，PLL的输出范围同样是650MHZ到1300MHZ。此路PLL在最终输出的时候还可以进行分频，可选1、2、4分频。

# 2. 时钟树
芯片手册给出的一张时钟树的图片，非常详细，从这张时钟树上可以看出各种外设的时钟来源。时钟树在参考手册的第18章。

![](./images/Snipaste_2024-10-31_22-24-48.png)

上图中黄色字体的是复用选择寄存器，比如：

```
CSCMR1[USDHCn_CLK_SEL]， 其中 CSCMR1 为寄存器名，USDHCn_CKL_SEL 为该寄存器中的 bit 位。
```

而上图中红色字体分频选择寄存器，比如：

```
CSCDR1[USDHCn_PODF]，其中 CSCDR1 为寄存器名，USDHCn_PODF 为该寄存器中的 bit 位。
```

上图一共分为三部分，CLOCK_SWITCHER、CLOCK_ROOT GENERATOR、SYSTEM CLOCKS。其中左边的 CLOCK_SWITCHER 就是上一小节讲的那7路PLL和8路PDF，右边的 SYSTEM CLOCKS 就是芯片外设，中间的 CLOCK_ROOT GENERATOR 是最复杂的，通过它来连接左边的输入和右边的输出。

以 ESAI 的时钟图为例，如下图：

![](./images/Snipaste_2024-10-31_22-51-15.png)

- (1) 此部分是时钟源选择器，ESAI有4个可选的时钟源：PLL4、PLL5、PLL3_PFD2、pll3_sw_clk。具体选择哪一路作为 ESAI 的时钟源是由寄存器 CCM->CSCMR2 的 ESA_CLK_SEL 位来决定的，用户可以自由配置，如下图：

![](./images/Snipaste_2024-10-31_22-53-01.png)

- (2) 此部分是 ESAI 时钟的前级分频，分频值由寄存器 CCM_CS1CDR 的 ESAI_CKL_PRED 位来确定的，可设置 1 - 8 分频。
- (3) 此部分又是一个分频器，对第2步的输出时钟进一步分频，分频值由寄存器 CCM_CS1CDR 的 ESAI_CKL_PODF 来决定，可设置 1 - 8 分频。

# 3. 内核时钟设置
从时钟树可以看到 ARM 内核时钟如下图：

![](./images/Snipaste_2024-10-31_23-23-14.png)

- (1). 内核时钟源来自于PLL1
- (2). 通过寄存器 CCM_CACRR 的 ARM_PODF 位对PLL1进行分频，可选择1/2/4/8分频
- (3). 这里固定为2分频，实际上并没有分配，这里注意一下
- (4). 这里就是输出到ARM内核的时钟源

由上面分析可知，如果我们想设置I.MX6ULL的主频为 528MHZ的话。假设PLL1频率为1056MHZ，那么只需要设置寄存器 CCM_CACRR 的 ARM_PODF 位为 2 分频即可。接下来先看一下 CCM_CACRR 寄存器：

![](./images/Snipaste_2024-10-31_23-30-26.png)

寄存器 CCM_CACRR 只有 ARM_PODF 位，可以设置 0-7，分别对应1-8分频，如果设置为2分频的话，则 ARM_PODF 的值就设置为 1 即可。

好了，现在有另一个问题，就是 PLL1 的时钟频率怎么设置成 1056MHZ 呢？答案是：控制 PLL1 的寄存器为 CCM_ANALOG_PLL_ARMn （这个寄存器是怎么确定的，我也不知道，看正点原子的文档上写的）。接下来就看一下该寄存器的内容：

![](./images/Snipaste_2024-10-31_23-34-53.png)

寄存器 CCM_ANALOG_PLL_ARMn 的重要的位如下：

- `ENABLE`: 时钟输出使能位，此位设置为1使能PLL1的输出，如果设置为0则关闭PLL1的输出。
- `DIV_SELECT`: 此位设置PLL1的输出频率，可设置的范围为 54-108， PLL1 CLK = Fin * div_sect/2.0，其中 Fin=24MHZ。所以，如果想让PLL1输出频率为1056MHZ，那么 div_select 的值就设置为 88 即可。

在修改PLL1时钟频率的时候，我们需要先将内核时钟源改为其它的时钟源，设置完PLL1的参数后，再把内核时钟源切换回来。PLL1可选择的时钟源如下图：

![](./images/Snipaste_2024-10-31_23-40-26.png)

- (1) pll1_sw_clk 也就是 PLL1 的最终输出频率
- (2) 此处是一个选择器，选择 pll1_sw_clk 的时钟源，由寄存器 CCM_CCSR 的 PLL1_SW_CKL_SEL 位决定 pll1_sw_clk 是选择 pll1_main_clk 还是 step_clk。正常情况下应该选择 pll1_main_clk，但是如果要对 pll1_main_clk(PLL1) 的频率进行调整的话，此时需要先将 pll1_sw_clk 切换到 step_clk 上。等 pll1_main_clk 调整完成后再切换回来。
- (3) 此处也是一个选择器，选择 step_clk 的时钟源，由寄存器 CCM_CCSR 的 STEP_SEL 位决定 step_clk 是选择 osc_clk 还是 secondary_clk。

总结一下：设置内核时钟源的步骤如下：

     1. 设置寄存器 CCM_CCSR 的 STEP_SEL 位，设置 step_clk 的时钟源为 24MHZ 晶振。
     2. 设置寄存器 CCM_CCSR 的 PLL1_SW_CKL_SET 位，设置 pll1_sw_clk 的时钟源为 step_clk。
     3. 设置寄存器 CCM_ANALOG_PLL_ARMn 的 DIV_SELECT 位为 88，即 pll1_main_clk 为 1056MHZ。
     4. 设置寄存器 CCM_CCSR 的 PLL1_SW_CLK_SEL 位，重新将 pll1_sw_clk 的时钟源切换回 pll1_main_clk，切换回来以后的 pll1_sw_clk 就等于 1056MHZ 了。
     5. 最后设置寄存器 CCM_CACRR 的 ARM_PODF 位为 1（2分频），即 ARM 内核时钟输出为 1056MHZ/2=528MHZ。

# 4. PFD时钟设置
设置好主频以后，还需要设置其它路的PLL及PFD时钟。

PLL2, PLL3, PLL7 这三路的频率是固定的，分别为528，480，480MHZ。PLL4-PLL6都是针对特殊外设的，用到的时候再设置。因此，接下来就是设置 PLL2 和 PLL3 各自的 4 路 PFD。各路官方推荐的频率值如下：

![](./images/Snipaste_2024-11-01_09-15-40.png)

先设置 PLL2 的4路 PFD 的频率，使用到的寄存器是 `CCM_ANALOG_PFD_528n`，其内容如下：

![](./images/Snipaste_2024-11-01_09-18-21.png)

从上图可以看出，寄存器 CCM_ANALOG_PFD_528n 分为4组，分别对应 `PFD0-PFD3`，每组8bit。现以 PFD0 为例，看一下如何设置 PLL2_PFD0 的频率。PFD0 对应的寄存器位如下：

- `PFD0_FRAC`: PLL2_PFD0的分频数，PLL2_PFD0的计算公式为 `528*18/PFD0_FRAC`，此位可设置的范围为 12-35。
- `PFD0_STABLE`: 此位为只读位，可以通过读取此位判断PLL2_PFD0是否稳定。
- `PFD0_CLKGATE`: 此位为1时，PLL2_PFD0 的时钟输出被关闭，为0时，PLL2_PFD0 的时钟输出被开启。

接下来再设置 PLL3 的4路 PFD 的频率，使用到的寄存器是 `CCM_ANALOG_PFD_480n`，其内容如下：

![](./images/Snipaste_2024-11-01_09-24-40.png)

从上图可以看出，该寄存器的结构与 CCM_ANALOG_PFD_528n 一样。只是频率输出的计算公式不同，比如 PLL3_PFD0 的频率计算公式为 `480*18/PFD0_FRAC`。其它不赘述。

# 5. 根时钟设置
根时钟对应在第2节中的时钟树框图中的 `CLOCK ROOT GENERATOR` 的靠右侧的部分，如 `MMDC_CLK_ROOT, AXI_CLK_ROOT, AHB_CLK_ROOT, IPG_CLK_ROOT...`，其实就是对应的外设时钟。I.MX6ULL外设根时钟可设置的范围如下表：

![](./images/Snipaste_2024-11-01_09-32-34.png)

现以 `AHB_CLK_ROOT` 了解下其设置过程。

AHB_CLK_ROOT 设置，需要看一下时钟树，如下图：

![](./images/Snipaste_2024-11-01_09-45-18.png)

从上图可以看出，AHB_CLK_ROOT 的来源可以有很多，上图中的 `1,2,3,4,5`就是其中的路径之一，路径上涉及到的各个寄存器都已标明，查看对应的寄存器即可。

# 6. MMDC握手
在修改如下时钟选择器或者分频器的时候会引起与 MMDC 的握手发生：

```
1. mmdc_podf
2. periph_clk_sel
3. periph2_clk_sel
4. arm_podf
5. ahb_podf
```

发生握手信号以后需要等待握手完成，寄存器 CCM_CDHIPR 中保存着握手信号是否完成，如果相应的位为1则表示握手未完成；如果为0表示握手完成。

# 7. 其它
如果在设置时钟频率的过程中发现输出不正确，除了核对相关的寄存器是否设置正确之外，还需要知道除了我们自己的配置，在开机时 boot ram 也会对时钟进行初始化，可以分析一下 IVT, DCD 这些数据。
