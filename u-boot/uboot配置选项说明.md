[TOC]

# 非Kconfig的配置项
- CONFIG_ARMV7_LPAE：用于启用 ARMv7 架构上的 Large Physical Address Extension (LPAE) 的配置选项。以下是该配置的具体作用：

    1. 启用 LPAE 支持：LPAE 允许 ARMv7 处理器支持超过 4GB 的物理内存。通过启用这个配置，U-Boot 可以管理更大的物理地址空间。
    2. 修改页表结构：LPAE 引入了新的页表结构和地址翻译机制，使得每个页表项可以表示更大的物理地址范围。
    3. 提高内存管理效率：在处理大内存系统时，LPAE 可以提高内存管理的效率和性能。

# Kconfig配置选项
- CONFIG_BOARD_EARLY_INIT_F：有些 board 需要在uboot启动后立即执行一些动作，则配置了该选项后，在uboot重定位前并且初始化完成 driver model 后就会调用 board_early_init_f 这个函数。例如 I.MX6ULL 就在这个函数中初始化了串口。

