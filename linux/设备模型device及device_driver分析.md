本文分析基于内核版本 6.12.0

[TOC]

# 1. device及deivce_driver数据结构
只列出该结构定义在哪个文件中吧，具体定义自行查看源码吧。

```c
File: include\linux\device.h
721: struct device 

File: include\linux\device\driver.h
96: struct device_driver
```
# 2. device的注册
内核提供很多struct device结构的操作接口（具体可以参考include/linux/device.h和drivers/base/core.c的代码）。这里我们以 `device_register()`函数看一下其流程。

```c
File: drivers\base\core.c
3751: int device_register(struct device *dev)
3752: {
3753: 	device_initialize(dev);
3754: 	return device_add(dev);
3755: }
```

调用该函数前首先用户需要先自行动态分配一个 struct device 结构。当然device模块也提供了由内核自动分配并初始化device结构的函数，如 `device_create()`。我们就以`device_register()`函数为主分析一下。

从上面的代码可以看到，device_register 函数首先调用了 device_initialize 函数。device_initialize函数主要就是对 dev 成员做初始化。其函数定义如下：

```c
File: drivers\base\core.c
3139: void device_initialize(struct device *dev)
3140: {
        //赋值kset结构，想要上报event就必须依附一个kset
3141: 	dev->kobj.kset = devices_kset;
        /*
        调用kobject_init函数,向内核注册dev->kobj结构，该函数作用请参考《统一设备模型分析.md》文档。有详细说明。
        此时 dev->kobj->ktype = device_ktype，想要读写属性文件，必须要有一个ktype
        */
3142: 	kobject_init(&dev->kobj, &device_ktype);
3143: 	INIT_LIST_HEAD(&dev->dma_pools);
3144: 	mutex_init(&dev->mutex);
3145: 	lockdep_set_novalidate_class(&dev->mutex);
3146: 	spin_lock_init(&dev->devres_lock);
3147: 	INIT_LIST_HEAD(&dev->devres_head);
3148: 	device_pm_init(dev);
3149: 	set_dev_node(dev, NUMA_NO_NODE);
3150: 	INIT_LIST_HEAD(&dev->links.consumers);
3151: 	INIT_LIST_HEAD(&dev->links.suppliers);
3152: 	INIT_LIST_HEAD(&dev->links.defer_sync);
3153: 	dev->links.status = DL_DEV_NO_DRIVER;
3154: #if defined(CONFIG_ARCH_HAS_SYNC_DMA_FOR_DEVICE) || \
3155:     defined(CONFIG_ARCH_HAS_SYNC_DMA_FOR_CPU) || \
3156:     defined(CONFIG_ARCH_HAS_SYNC_DMA_FOR_CPU_ALL)
3157: 	dev->dma_coherent = dma_default_coherent;
3158: #endif
3159: 	swiotlb_dev_init(dev);
3160: }
```

上述代码的第3142行再说明一下，调用kobject_init后，会把device_ktype指针赋值给dev->kobj->ktype。作用就是用于访问该kobj下的属性文件，怎么做到的？其实在《统一设备模型分析.md》文章中的第"3.4 attribute文件的读写"的流程图中我们就以struct class为例说明了其访问class下的属性文件的流程了。这里又是一个例子，再次看一下吧。

device_ktype 的定义如下：

```c
File: drivers\base\core.c
2600: static const struct kobj_type device_ktype = {
2601: 	.release	= device_release,
2602: 	.sysfs_ops	= &dev_sysfs_ops,
2603: 	.namespace	= device_namespace,
2604: 	.get_ownership	= device_get_ownership,
2605: };
```
其中的.sysfs_ops的成员又被赋值为 dev_sysfs_ops 的地址，所以再看 dev_sysfs_ops 的定义：

```c
File: drivers\base\core.c
2450: static const struct sysfs_ops dev_sysfs_ops = {
2451: 	.show	= dev_attr_show,
2452: 	.store	= dev_attr_store,
2453: };
```

所以读操作最终会调用到 dev_attr_show 函数，该函数是访问对应的kobj的目录级别，要想访问该kobj目录下的文件，还需要一次转接，所以再看一下 dev_attr_show 函数的实现：

```c
File: drivers\base\core.c
2422: static ssize_t dev_attr_show(struct kobject *kobj, struct attribute *attr,
2423: 			     char *buf)
2424: {
2425: 	struct device_attribute *dev_attr = to_dev_attr(attr);  //通过入参attr得到device_attribute结构
2426: 	struct device *dev = kobj_to_dev(kobj);
2427: 	ssize_t ret = -EIO;
2428: 
2429: 	if (dev_attr->show)
2430: 		ret = dev_attr->show(dev, dev_attr, buf);       //调用device_attribute结构中的show函数
2431: 	if (ret >= (ssize_t)PAGE_SIZE) {
2432: 		printk("dev_attr_show: %pS returned bad count\n",
2433: 				dev_attr->show);
2434: 	}
2435: 	return ret;
2436: }
```
从 dev_attr_show 函数实现可以看出，该函数就是完成最后的转接，目的就是访问到最终的属性文件。而 device_attribute 结构中的 show, store 函数就是由驱动程序员去实现，用于访问包含device结构的大型数据结构中的成员变量了。

整个调用过程大致为(以读为例)：`VFS --> kernfs --> kernfs->ops --> sysfs_ops --> .show --> xxx_attribute.show`，其中kobj_type、sysfs_ops和xxx_attribute都是由包含kobject的上层数据结构实现。

好了，插曲讲完，回到 `device_initialize` 函数，接下来就是device结构的其它成员初始化了，没什么看的，所以该函数就结构了。回到 `device_register`函数中开始执行`device_add`函数。

device_add 函数内容太多了，这里直接说一下主要的流程：

- 1 增加该device中的kobj的引用计数，对应代码为 `dev = get_device(dev)` 
- 2 如果dev->p不为空，则初始化dev->p的结构，dev->p的结构为 struct device_private，调用函数 `device_private_init(dev)`
- 3 如果dev->init_name设置的有名字，则调用 `dev_set_name(dev, "%s", dev->init_name)`设置dev->kobj.name
- 4 如果初始化的时候没有指定名字，则由内核根据"bus name + device ID"的方式构造名字，调用函数为 `dev_set_name(dev, "%s%u", dev->bus->dev_name, dev->id)`
- 5 增加dev->parent->kobj的引用计数，对应调用的代码为 `get_device(dev->parent)`
- 6 将dev所包含的kobj添加到内核，调用 `kobject_add(&dev->kobj, dev->kobj.parent, NULL)`，该函数会在sysfs中创建目录，并创建默认属性文件。参考《统一设备模型分析.md》文章
- 7 创建软链接，对应代码 `device_platform_notify(dev)`
- 8 创建属性文件，调用函数 `device_create_file(dev, &dev_attr_uevent)`

这一步需要展开分析一下，这里调用了 `device_create_file(dev, &dev_attr_uevent)` 即创建该dev对应的属性文件，我们在《统一设备模型分析.md》文章中的第"3.3 attribute文件的创建"讲过attribute文件的创建，当时流程图是从 `sysfs_create_files()` 函数开始的，那么这里可以猜测 device_create_file 函数内部应该是调用了 sysfs_create_files 函数以完成属性文件的创建。接下来看一下 device_create_file 函数的实现，来验证下猜测。

```c
File: drivers\base\core.c
3026: int device_create_file(struct device *dev,
3027: 		       const struct device_attribute *attr)
3028: {
3029: 	int error = 0;
3030: 
3031: 	if (dev) {
3032: 		WARN(((attr->attr.mode & S_IWUGO) && !attr->store),
3033: 			"Attribute %s: write permission without 'store'\n",
3034: 			attr->attr.name);
3035: 		WARN(((attr->attr.mode & S_IRUGO) && !attr->show),
3036: 			"Attribute %s: read permission without 'show'\n",
3037: 			attr->attr.name);
3038: 		error = sysfs_create_file(&dev->kobj, &attr->attr);
3039: 	}
3040: 
3041: 	return error;
3042: }
```
可以看到，device_create_file 函数中的有效代码就是直接调用了 sysfs_create_file 函数。所以相了解创建属性文件的过程请看《统一设备模型分析.md》文章中的第"3.3 attribute文件的创建"的流程图。

接下来再看一下 device_create_fileb函数的第二个入参，它是`dev_attr_uevent`，看一下它的定义：

```c
File: drivers\base\core.c
2765: static DEVICE_ATTR_RW(uevent);

宏展开：
File: include\linux\device.h
180: #define DEVICE_ATTR_RW(_name) \
181: 	struct device_attribute dev_attr_##_name = __ATTR_RW(_name)

#define __ATTR_RW(_name) __ATTR(_name, 0644, _name##_show, _name##_store)

#define __ATTR(_name, _mode, _show, _store) {				\
	.attr = {.name = __stringify(_name),				\
		 .mode = VERIFY_OCTAL_PERMISSIONS(_mode) },		\
	.show	= _show,						\
	.store	= _store,						\
}

最终展开为：
struct device_attribute dev_attr_uevent = {
        .attr = {
                .name = "uevent", 
                .mode = 0644 },
        .show = uevent_show,
        .store = uevent_store
}
```
所以说最终操作该dev的属性文件的函数就是 uevent_show 和 uevent_store 函数了。

device的注册函数不再向下分析了，直接给出调用流程图如下：

![](./images/Snipaste_2024-12-11_22-53-13.png)

注意上图中红字的部分，那里调用了 bus->match，drv->probe 等函数。如果图片不清晰，其对应的源文件为 "draw/device及device_driver注册流程.png"

# 3. driver的注册
driver的注册是调用 `driver_register` 函数。

```c
File: drivers\base\driver.c
222: int driver_register(struct device_driver *drv)
223: {
224: 	int ret;
225: 	struct device_driver *other;
226: 
        /*
        判断drv所依赖的bus是否已注册。因为驱动正常运行的前提是其所依赖的bus已准备好
        */
227: 	if (!bus_is_registered(drv->bus)) {
228: 		pr_err("Driver '%s' was unable to register with bus_type '%s' because the bus was not initialized.\n",
229: 			   drv->name, drv->bus->name);
230: 		return -EINVAL;
231: 	}
232: 
233: 	if ((drv->bus->probe && drv->probe) ||
234: 	    (drv->bus->remove && drv->remove) ||
235: 	    (drv->bus->shutdown && drv->shutdown))
236: 		pr_warn("Driver '%s' needs updating - please use "
237: 			"bus_type methods\n", drv->name);
238: 
        //不能注册同名的driver
239: 	other = driver_find(drv->name, drv->bus);
240: 	if (other) {
241: 		pr_err("Error: Driver '%s' is already registered, "
242: 			"aborting...\n", drv->name);
243: 		return -EBUSY;
244: 	}
245: 
246: 	ret = bus_add_driver(drv);  //该函数最终会调用 dev->bus->probe（如果存在），或者调用 drv->probe函数
247: 	if (ret)
248: 		return ret;
249: 	ret = driver_add_groups(drv, drv->groups);
250: 	if (ret) {
251: 		bus_remove_driver(drv);
252: 		return ret;
253: 	}
254: 	kobject_uevent(&drv->p->kobj, KOBJ_ADD);
255: 	deferred_probe_extend_timeout();
256: 
257: 	return ret;
258: }
```

上面函数中 `bus_add_driver` 还是比较重要的，因为它会最终调用 dev->bus->probe 或 drv->probe 函数。该函数就是我们驱动程序员写的驱动入口函数。具体调用过程的流程图如下：

![](./images/Snipaste_2024-12-11_22-15-32.png)

注意上图中红字的部分，那里调用了 bus->match，drv->probe 等函数。如果图片不清晰，其对应的源文件为 "draw/device及device_driver注册流程.png"

# 4. 设备驱动probe的时机
设备驱动probe的时机有如下几种(分为自动触发和手动触发)：
- 将struct device类型的变量注册到内核时自动触发(在第2节的流程图中可以看到执行了probe)
- 将struct device_driver类型的变量注册到内核时自动触发(在第3节的流程图中可以看到执行了probe)
- 手动查找同一bus下的所有device_driver，如果有和指定device同名的driver，执行probe操作(device_attach)
- 手动查找同一bus下的所有device，如果有和指定driver同名的device，执行probe操作(driver_attach)
- 自行调用driver的probe接口，并在该接口中将该driver绑定到某个device结构中，即设置dev->driver（device_bind_driver）

注：每个bus都有一个drivers_autoprobe变量，用于控制是否在device或者driver注册时，自动probe。该变量默认为1（即自动probe），bus模块将它开放到sysfs中了，因而可在用户空间修改，进而控制probe行为。 