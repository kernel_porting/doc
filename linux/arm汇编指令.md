[TOC]

# THUMB( ... )
如果当前编译模式是 Thumb 模式，则 THUMB(...) 括号中的汇编指令会编译成 Thumb 指令，否则不编译该指令。

# adr_l
adr_l 是 ARM 汇编语言中的一个伪指令，用于生成一个 32 位的绝对地址，并将其加载到目标寄存器中。语法如下：

```asm
adr_l <target_register>, <label>
```

# mvn
mvn：这是 ARM 汇编语言中的“位非”（bitwise NOT）指令。例如：

```
mvn r1, #0      //即为0按位取反，然后把值赋值给r1，即r1的值为 0xFFFFFFFF
```

# .pushsection/.popsection
.pushsection 和 .popsection 是汇编语言中的伪指令，用于在编译过程中临时切换当前的输出段（section）。它们在处理复杂的链接脚本或需要在多个段之间跳转时非常有用。

```
.pushsection
    功能：将当前的输出段（section）压入一个栈，并切换到指定的新段。
    语法：.pushsection section_name, "flags"
        section_name：目标段的名称。
        "flags"：可选参数，用于指定段的属性，如读写权限等 。
        示例：.pushsection .text, "ax"

.popsection
    功能：从栈中弹出最近一次被压入的段，并恢复为当前的输出段。
    语法：.popsection
```

# orr
orr 是 ARM 汇编语言中的一条指令，用于执行按位或（OR）操作。它将两个操作数进行按位或运算，并将结果存储到目标寄存器中。

```
orr <Rd>, <Rn>, <Rm>
<Rd>：目标寄存器，存储结果。
<Rn>：第一个操作数寄存器。
<Rm>：第二个操作数寄存器。
orr 指令将 <Rn> 和 <Rm> 中的值进行按位或运算，并将结果存储到 <Rd> 中。
```

# .type
.type 指令在汇编语言中用于指定符号的类型。在 ARM 汇编中，.type 指令通常用于告诉链接器或调试器某个符号的类型。

```
.type 指令：

    语法：.type symbol, type
        symbol：符号的名称。
        type：符号的类型，可以是 #object、#function、#label 等。
        
        #object 类型：
            #object 表示该符号是一个数据对象，通常是一个变量或数据结构。
            这对于链接器和调试器来说非常重要，因为它们需要知道符号的类型来进行正确的处理。
```

# .size
.size 指令在汇编语言中用于指定符号的大小。在 ARM 汇编中，.size 指令通常用于告诉链接器或调试器某个符号的大小。

```
.size 指令：
    语法：.size symbol, size
        symbol：符号的名称。
        size：符号的大小，通常是一个表达式。
```

# .macro
.macro 指令用于定义宏（macro）。宏是一种预处理指令，允许你在汇编代码中定义一段可重用的代码块，并在需要时通过简单的调用将其展开。这可以提高代码的可读性和可维护性。

```
.macro <macro_name> [arg1, arg2, ...]
    ; 宏体代码
.endm
```

可能还会见到如下写法，比如有两个参数：

```
.macro <macro_name> arg1:req, arg2
    ; 宏体代码
.endm
```

上述示例中的 arg1:req 表示第一个参数是必须的(:req 的作用)，而 arg2 则是可选的。