本文分析基本linux内核源码版本 6.12.0

[TOC]

# 1. bus数据结构
在Linux设备模型中，Bus（总线）是一类特殊的设备，它是连接处理器和其它设备之间的通道（channel）。为了方便设备模型的实现，内核规定，系统中的每个设备都要连接在一个Bus上，这个Bus可以是一个内部Bus、虚拟Bus或者Platform Bus。

```c
File: include\linux\device\bus.h
077: struct bus_type {
078: 	const char		*name;
079: 	const char		*dev_name;
080: 	const struct attribute_group **bus_groups;
081: 	const struct attribute_group **dev_groups;
082: 	const struct attribute_group **drv_groups;
083: 
084: 	int (*match)(struct device *dev, const struct device_driver *drv);
085: 	int (*uevent)(const struct device *dev, struct kobj_uevent_env *env);
086: 	int (*probe)(struct device *dev);
087: 	void (*sync_state)(struct device *dev);
088: 	void (*remove)(struct device *dev);
089: 	void (*shutdown)(struct device *dev);
090: 
091: 	int (*online)(struct device *dev);
092: 	int (*offline)(struct device *dev);
093: 
094: 	int (*suspend)(struct device *dev, pm_message_t state);
095: 	int (*resume)(struct device *dev);
096: 
097: 	int (*num_vf)(struct device *dev);
098: 
099: 	int (*dma_configure)(struct device *dev);
100: 	void (*dma_cleanup)(struct device *dev);
101: 
102: 	const struct dev_pm_ops *pm;
103: 
104: 	bool need_parent_lock;
105: };
```
`bus_type`结构中的各个成员的含义在内核源码中都有注释，这里我就不翻译了，直接看内核源码即可。这里有一点要提一下，在旧版本中，bus_type结构中还包含一个结构叫做 `struct subsys_private`,但是正如上面展示的bus_type结构中并不存在该struct subsys_private结构。可以看一下bus_type结构中并没有包含kobj,kset等结构，那bus_type是怎么融入到设备模型中勒？其实还是通过struct subsys_private这个结构来实现的。我们来看一下它的结构内容：

```c
File: drivers\base\base.h
42: struct subsys_private {
43: 	struct kset subsys;
44: 	struct kset *devices_kset;
45: 	struct list_head interfaces;
46: 	struct mutex mutex;
47: 
48: 	struct kset *drivers_kset;
49: 	struct klist klist_devices;
50: 	struct klist klist_drivers;
51: 	struct blocking_notifier_head bus_notifier;
52: 	unsigned int drivers_autoprobe:1;
53: 	const struct bus_type *bus;
54: 	struct device *dev_root;
55: 
56: 	struct kset glue_dirs;
57: 	const struct class *class;
58: 
59: 	struct lock_class_key lock_key;
60: };
```
该结构和device_driver中的struct driver_private类似(可以回头去看一下device_driver结构，其中就包含了 driver_private结构，该结构中包含Kobj结构，可以与设备模型连接起来)。subsys_private用于指向bus_type/class驱动核心部分的私有数据结构。

各成员的基本含义如下：

- subsys: 代表本bus（如 sys/bus/spi），可以理解为每一个sys/bus或sys/class下面的目录都是一个子系统
- device_kset: 子系统下面的一个目录，目录名为devices，比如 sys/bus/spi/devices
- interfaces: 与该子系统相关的interface列表
- drivers_kset: 子系统下面的一个目录，目录名为drivers，比如 sys/bus/spi/drivers
- klist_devices/klist_drivers: 挂接在该bus下的设备与驱动列表，用于遍历查找
- bus_notifier: 用于通知bus相关的事件
- drivers_autoprobe: 是否自动探测驱动，如果为1则在device或driver被注册时执行probe函数
- bus: 指向当前的bus_type结构
- dev_root: device默认使用的parent
- class: 指赂当前的class结构

上面还有一个新的结构类型 struct klist，定义如下：

```c
File: include\linux\klist.h
37: struct klist_node {
38: 	void			*n_klist;	/* never access directly */
39: 	struct list_head	n_node;
40: 	struct kref		n_ref;
41: };

18: struct klist {
19: 	spinlock_t		k_lock;
20: 	struct list_head	k_list;
21: 	void			(*get)(struct klist_node *);
22: 	void			(*put)(struct klist_node *);
23: } __attribute__ ((aligned (sizeof(void *))));
```
如果 klist_node 结构将嵌入到引用计数对象中（这是安全删除所必需的），那么 get/put 参数将用于初始化获取和释放嵌入对象引用计数。


# 2. bus的注册
bus的注册函数是 bus_register()，该函数位于 drivers/base/bus.c 文件中。直接看该函数的调用流程图：

![](./draw/bus_register函数调用流程.png)

从上面的流程图可以看出，每条bus下都会默认创建两个目录及三个属性文件，目录为 drivers/ 和 devices/， 文件为 uevent, drivers_probe, drivers_autoprobe。

# 3. device和driver的添加
经过第2节的分析，可知每条bus下都会存在 drivers/ 和 devices/ 两个目录，这两个目录的作用就是存放挂接在该bus下的所有device和driver。那针对具体的device和driver，如何挂接在该bus下呢？bus模块提供以下两个接口用于device和driver添加到bus中，接口如下：

```c
File: drivers\base\base.h
150: int bus_add_device(struct device *dev);
156: int bus_add_driver(struct device_driver *drv);
```

回忆一下，在《device及device_driver分析.md》文章中提到，device注册是调用 device_register()函数，所谓的注册说简单点就是把该device挂接到对应的bus上，所以在 device_register() 函数中就会调用 `bus_add_device()`函数；同理，driver注册是调用 driver_register() 函数，所以 driver_register() 函数中会调用 `bus_add_driver()` 函数。

这里我们再详细分析一下这两个接口。

`bus_add_device()`接口的调用流程如下，在该接口中涉及到device到bus目录下的软链接的创建等动作。

![](./images/Snipaste_2024-12-12_22-13-40.png)

再来看一下 `bus_add_driver()` 函数的调用流程：

![](./images/Snipaste_2024-12-12_23-18-08.png)

如果图片不清楚，源文件为 `draw/向bus中添加device与driver.png`


参考：[http://www.wowotech.net/device_model/bus.html](http://www.wowotech.net/device_model/bus.html)