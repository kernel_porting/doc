[TOC]

# 1. s_init函数移植
s_init 函数完成最早期的初始化，它只执行启动所需的最基本操作；不应该包括：设置DRAM；使用global_data；清除BSS；尝试启动一个console。

全局搜索 s_init，发现在 `arch/arm/mach-imx/mx6/soc.c` 文件中定义，在《01.imx6ull移植框架搭建》文章中有提到过，我们不再使用 machine 的概念，而是把SOC放到了 `arch/{arch}/cpu/{cpu}/{soc}/` 这个目录下；所以我们在目录 `arch/arm/cpu/armv7/` 目录下创建 mx6 目录，然后在该目录中创建 soc.c 文件，然后在该文件中定义 s_init 函数。(当前移植的代码中我们没有定义s_init这个函数，但编译还是通过了，原因是 s_init 被 weak 符号修饰，所以当没有明确定义 s_init 时，会使用默认的 s_init函数)。

![](./images/Snipaste_2024-10-24_21-43-40.png)

先来看一下 s_init 函数的实现吧：（这个代码直接从 `arch/arm/mach-imx/mx6/soc.c`中移植过来即可）

```c
File: arch\arm\cpu\armv7\mx6\soc.c   --------------------- (A)
07: void s_init(void)
08: {
09: 	struct anatop_regs *anatop = (struct anatop_regs *)ANATOP_BASE_ADDR;
10: 	struct mxc_ccm_reg *ccm = (struct mxc_ccm_reg *)CCM_BASE_ADDR;
11: 	u32 mask480;
12: 	u32 mask528;
13: 	u32 reg, periph1, periph2;
14: 
15: 	if (is_mx6sx() || is_mx6ul() || is_mx6ull() || is_mx6sll())
16: 		return;
17: 
18: 	/* Due to hardware limitation, on MX6Q we need to gate/ungate all PFDs
19: 	 * to make sure PFD is working right, otherwise, PFDs may
20: 	 * not output clock after reset, MX6DL and MX6SL have added 396M pfd
21: 	 * workaround in ROM code, as bus clock need it
22: 	 */
23: 
24: 	mask480 = ANATOP_PFD_CLKGATE_MASK(0) |
25: 		ANATOP_PFD_CLKGATE_MASK(1) |
26: 		ANATOP_PFD_CLKGATE_MASK(2) |
27: 		ANATOP_PFD_CLKGATE_MASK(3);
28: 	mask528 = ANATOP_PFD_CLKGATE_MASK(1) |
29: 		ANATOP_PFD_CLKGATE_MASK(3);
30: 
31: 	reg = readl(&ccm->cbcmr);
32: 	periph2 = ((reg & MXC_CCM_CBCMR_PRE_PERIPH2_CLK_SEL_MASK)
33: 		>> MXC_CCM_CBCMR_PRE_PERIPH2_CLK_SEL_OFFSET);
34: 	periph1 = ((reg & MXC_CCM_CBCMR_PRE_PERIPH_CLK_SEL_MASK)
35: 		>> MXC_CCM_CBCMR_PRE_PERIPH_CLK_SEL_OFFSET);
36: 
37: 	/* Checking if PLL2 PFD0 or PLL2 PFD2 is using for periph clock */
38: 	if ((periph2 != 0x2) && (periph1 != 0x2))
39: 		mask528 |= ANATOP_PFD_CLKGATE_MASK(0);
40: 
41: 	if ((periph2 != 0x1) && (periph1 != 0x1) &&
42: 		(periph2 != 0x3) && (periph1 != 0x3))
43: 		mask528 |= ANATOP_PFD_CLKGATE_MASK(2);
44: 
45: 	writel(mask480, &anatop->pfd_480_set);
46: 	writel(mask528, &anatop->pfd_528_set);
47: 	writel(mask480, &anatop->pfd_480_clr);
48: 	writel(mask528, &anatop->pfd_528_clr);
49: }
```

代码 A 的第15行，判断了一下当前的 CPU 是否是 mx6sx，mx6ul，mx6ull，mx6sll其中的一种，如果是则直接返回。因为我们的 cpu 是 imx6ull，所以直接返回了，也即 s_init 这个函数对于 imx6ull 是个空函数。

下面的分析可以不用看了。

代码 A 的第09行，看一下这个 `struct anatop_regs` 结构体是什么？定义如下：

```c
File: arch\arm\include\asm\arch\imx-regs.h
886: struct anatop_regs {
887: 	u32	pll_sys;		/* 0x000 */
888: 	u32	pll_sys_set;		/* 0x004 */
889: 	u32	pll_sys_clr;		/* 0x008 */
890: 	u32	pll_sys_tog;		/* 0x00c */
891: 	u32	usb1_pll_480_ctrl;	/* 0x010 */
892: 	u32	usb1_pll_480_ctrl_set;	/* 0x014 */
893: 	u32	usb1_pll_480_ctrl_clr;	/* 0x018 */
894: 	u32	usb1_pll_480_ctrl_tog;	/* 0x01c */
895: 	u32	usb2_pll_480_ctrl;	/* 0x020 */
...
```

可以看出，这个结构体中定义的都是寄存器的偏移地址，其相对地址为 ANATOP_BASE_ADDR；所以看一下 ANATOP_BASE_ADDR 的值是多少？

```c
File: arch\arm\include\asm\arch\imx-regs.h
192: #define ANATOP_BASE_ADDR            (AIPS1_OFF_BASE_ADDR + 0x48000)

File: arch\arm\include\asm\arch\imx-regs.h
171: #define AIPS1_OFF_BASE_ADDR         (ATZ1_BASE_ADDR + 0x80000)

File: arch\arm\include\asm\arch\imx-regs.h
130: #define ATZ1_BASE_ADDR              AIPS1_ARB_BASE_ADDR

File: arch\arm\include\asm\arch\imx-regs.h
77: #define AIPS1_ARB_BASE_ADDR             0x02000000
```

所以最终 ANATOP_BASE_ADDR = 0x02000000 + 0x80000 + 0x48000 = 0x020C8000；

    注：AIPS 指的 ARM Peripheral Interconnect (AIPS)。
    AIPS1 是 I.MX6ULL 中的一个 AIPS 实例，用于管理一组特定的外设。
    通过这个基地址(AIPS1_ARB_BASE_ADDR)，可以访问和配置 AIPS1 中的寄存器，以控制外设的访问权限和其他相关设置。

结合芯片手册：

![](./images/Snipaste_2024-10-24_22-30-08.png)

可以看到，首地址 0x020C8000 落在上图这个地址区间中。

再具体一点的地址如下：

![](./images/Snipaste_2024-10-24_22-40-03.png)

很遗憾，芯片手册没有找到 0x020C8000 的这段地址的相关描述，不清楚 MX6ULL 的具体实现，只能先放着，等后面有空再研究。

接着看代码 A 的第10行，定义了 `struct mxc_ccm_reg`结构，其首地址为 CCM_BASE_ADDR。

查看代码可以确实 CCM_BASE_ADDR 的值为 0x020C4000。该地址也属于 AIPS-1 区域。

![](./images/Snipaste_2024-10-24_22-59-12.png)

CCM 为时钟控制模块，其内存映射如下：

![](./images/Snipaste_2024-10-24_23-02-32.png)

可以看到，`struct mxc_ccm_reg`结构定义的成员与芯片手册列出的各个寄存器名是一一对应的。

# 2. 将 arch/arm/cpu/armv7/mx6/soc.c 添加编译

```c
File: arch\arm\Makefile
93: libs-y += arch/arm/cpu/$(CPU)/
```

上述 Makefile 中 libs-y 中包含了 `arch/arm/cpu/armv7/` 这个目录，所以在编译的时候会到该目录下查找 Makefile，所以我们打开该目录下的 Makefile，添加如下代码：

![](./images/Snipaste_2024-10-24_23-30-33.png)

经过前面分析，知道这里的 SOC 的值其实就是 mx6，所以这里的 obj-y 会包含 mx6/ 这个目录，所以最终编译时会进入到 `arch/arm/cpu/armv7/mx6/` 这个目录下查找 Makefile。

我们要编译 `arch/arm/cpu/armv7/mx6/soc.c` 文件，所以需要在 `arch/arm/cpu/armv7/mx6/` 目录下的 Makefile 添加如下代码：

![](./images/Snipaste_2024-10-24_23-33-17.png)

经过上面的修改，soc.c 文件就可以参与编译。然后在编译过程中遇到问题再解决问题。

# 3. 解决 soc.c 的编译错误
Makefile 修改好后，就可以直接编译了。果然不会这么顺序，报错如下：

![](./images/Snipaste_2024-10-25_09-43-35.png)

从上图可以看出，我们添加的 `arch/arm/cpu/armv7/mx6/soc.c` 这个文件已经被编译了。另外，报错信息提示 `get_cpu_rev` 该函数未被定义。报错的原因是因为代码 A 的第15行，其引用代码如下：

```c
File: arch\arm\include\asm\mach-imx\sys_proto.h
39: #define is_mx6ull() (is_cpu_type(MXC_CPU_MX6ULL))

File: arch\arm\include\asm\mach-imx\sys_proto.h
25: #define is_cpu_type(cpu) (get_cpu_type() == cpu)

File: arch\arm\include\asm\mach-imx\sys_proto.h
23: #define get_cpu_type() (cpu_type(get_cpu_rev()))

File: arch\arm\include\asm\mach-imx\sys_proto.h
20: #define cpu_type(rev) (((rev) >> 12) & 0xff)
```
上图是宏定义的调用关系，其中 `get_cpu_rev` 在我们移植的代码中没有定义。我们把 `arch/arm/mach-imx/mx6/soc.c`文件中的 get_cpu_rev 函数拷贝过来即可。

![](./images/Snipaste_2024-10-25_10-01-59.png)

然后重新编译即可通过。

s_init 函数执行完成后，程序会返回到 `arch/arm/cpu/armv7/start.S` 文件中的第88行，即 `bl _main`；所以会接着调用 _main 函数。

# 4. 函数调用流程
![](./images/Snipaste_2024-10-25_23-35-04.png)

