
[TOC]

《ARM ArchitectureReference Manual ARMv7-A and ARMv7-R edition.pdf》文档中的 `B8: The Generic Timer` 有对该定时器的介绍。另外，《Cortex-A7 Technical ReferenceManua.pdf》的 `Chapter 9 Generic Timer`也是对该定时器的介绍。

# 1. uboot对Generic Timer定时器的驱动
## 1.1 timer_init函数
在 uboot 重定位之前，在 `board_inif_f` 函数中会调用 `timer_init` 函数，该函数就是对 Generic Timer 的初始化。代码如下：

```c
File: arch\arm\imx-common\syscounter.c
59: int timer_init(void)
60: {
61: 	struct sctr_regs *sctr = (struct sctr_regs *)SCTR_BASE_ADDR;
62: 	unsigned long val, freq;
63: 
64: 	freq = CONFIG_SC_TIMER_CLK;
65: 	asm("mcr p15, 0, %0, c14, c0, 0" : : "r" (freq));
66: 
67: 	writel(freq, &sctr->cntfid0);
68: 
69: 	/* Enable system counter */
70: 	val = readl(&sctr->cntcr);
71: 	val &= ~(SC_CNTCR_FREQ0 | SC_CNTCR_FREQ1);
72: 	val |= SC_CNTCR_FREQ0 | SC_CNTCR_ENABLE | SC_CNTCR_HDBG;
73: 	writel(val, &sctr->cntcr);
74: 
75: 	gd->arch.tbl = 0;
76: 	gd->arch.tbu = 0;
77: 
78: 	return 0;
79: }
```

上面代码中的第61行的宏 SCTR_BASE_ADDR 的值为：

```
#define SCTR_BASE_ADDR              SYSCNT_CTRL_IPS_BASE_ADDR
#define SYSCNT_CTRL_IPS_BASE_ADDR   (AIPS2_OFF_BASE_ADDR + 0x5C000)
#define AIPS2_OFF_BASE_ADDR         (ATZ2_BASE_ADDR + 0x80000)
#define ATZ2_BASE_ADDR              AIPS2_ARB_BASE_ADDR
#define AIPS2_ARB_BASE_ADDR         0x02100000

0x02100000 + 0x80000 + 0x5C000 = 0x021DC000
```

由 I.MX6ULL 芯片手册可以确定该地址：

![](./images/Snipaste_2024-10-29_13-09-25.png)

代码的第64-65行，设置 CP15 协处理器中的 C14 寄存器的值为 CONFIG_SC_TIMER_CLK。该宏被定义为：8000000，即8MHz。由于 `opc1=0,CRn=c14,CRm=c0,opc2=0`，所以这个组合对应下图：

![](./images/Snipaste_2024-10-29_13-22-46.png)

所以，此时的 c14 寄存器代表的是 `Counter Frequency Register`。该寄存器如下：

![](./images/Snipaste_2024-10-29_13-28-41.png)


struct sctr_regs 结构及代码中使用的到其它宏定义如下：

```c
File: arch\arm\include\asm\imx-common\syscounter.h
10: /* System Counter */
11: struct sctr_regs {
12: 	u32 cntcr;
13: 	u32 cntsr;
14: 	u32 cntcv1;
15: 	u32 cntcv2;
16: 	u32 resv1[4];
17: 	u32 cntfid0;
18: 	u32 cntfid1;
19: 	u32 cntfid2;
20: 	u32 resv2[1001];
21: 	u32 counterid[1];
22: };
23: 
24: #define SC_CNTCR_ENABLE		(1 << 0)
25: #define SC_CNTCR_HDBG		(1 << 1)
26: #define SC_CNTCR_FREQ0		(1 << 8)
27: #define SC_CNTCR_FREQ1		(1 << 9)
```

在 I.MX6ULL 没有找到上述寄存器的定义，并不清楚 `struct sctr_regs` 结构中定义的寄存器都是什么；但从 `timer_init` 可以看出，就是设置频率，然后使能该定时器。

## 1.2 get_ticks函数
定时器初始化完成后，可以使用许多时间相关的函数，如下：

```c
File: arch\arm\imx-common\syscounter.c
81: unsigned long long get_ticks(void)
82: {
83: 	unsigned long long now;
84: 
85: 	asm("mrrc p15, 0, %Q0, %R0, c14" : "=r" (now));
86: 
87: 	gd->arch.tbl = (unsigned long)(now & 0xffffffff);
88: 	gd->arch.tbu = (unsigned long)(now >> 32);
89: 
90: 	return now;
91: }
```

第85行 `mrrc p15, 0, %Q0, %R0, c14` 这个命令是从 64 位的 c14 寄存器中读出定时器计数值，高32位存放在 R0, 低32位存放在 Q0。依据的文档说明如下：

![](./images/Snipaste_2024-10-29_14-32-10.png)

![](./images/Snipaste_2024-10-29_14-31-22.png)

`mrrc` 命令是从协处理器中的寄存器中读2个32位的数据放入到 ARM 寄存器中。另外，读64位的协处理器的寄存器时命令也有变化，命令中不再包含有 `opc2, CRm` 这两项了。

## 1.3 get_tbclk函数
```c
File: arch\arm\imx-common\syscounter.c
119: ulong get_tbclk(void)
120: {
121: 	unsigned long freq;
122: 
123: 	asm volatile("mrc p15, 0, %0, c14, c0, 0" : "=r" (freq));
124: 
125: 	return freq;
126: }
```

该函数很简单，就是从 c14 寄存器中读出时钟频率。由于我们在 `timer_init` 把时钟频率设置为 8MHz，所以该函数返回值为 8000000。也即该函数的功能就是：`返回该定时器每秒的滴答数`。

另外，syscounter.c 文件中还提供了其它的时钟操作函数，都是基于 System Counter 的，就不一一介绍了。


