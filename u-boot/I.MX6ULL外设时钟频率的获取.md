在 uboot 的 `board_init_f` 函数中会调用 `get_clocks` 函数，该函数会获取 I.MX6ULL 的各种外设时钟。接下来就看一下该函数的实现。

```c
File: arch\arm\imx-common\speed.c  ------------------------ (A)
19: int get_clocks(void)
20: {
21: #ifdef CONFIG_FSL_ESDHC
22: #ifdef CONFIG_FSL_USDHC
23: #if CONFIG_SYS_FSL_ESDHC_ADDR == USDHC2_BASE_ADDR
24: 	gd->arch.sdhc_clk = mxc_get_clock(MXC_ESDHC2_CLK);
...
31: #endif
...
43: #endif
44: 	return 0;
45: }
```

获取外设时钟频率的统一函数入口就是 `mxc_get_clock`，该函数根据不同的入参枚举返回不同的外设时钟。其枚举值如下：

```c
File: arch\arm\include\asm\arch-mx6\clock.h
25: enum mxc_clock {
26: 	MXC_ARM_CLK = 0,
27: 	MXC_PER_CLK,
28: 	MXC_AHB_CLK,
29: 	MXC_IPG_CLK,
30: 	MXC_IPG_PERCLK,
31: 	MXC_UART_CLK,
32: 	MXC_CSPI_CLK,
33: 	MXC_AXI_CLK,
34: 	MXC_EMI_SLOW_CLK,
35: 	MXC_DDR_CLK,
36: 	MXC_ESDHC_CLK,
37: 	MXC_ESDHC2_CLK,
38: 	MXC_ESDHC3_CLK,
39: 	MXC_ESDHC4_CLK,
40: 	MXC_SATA_CLK,
41: 	MXC_NFC_CLK,
42: 	MXC_I2C_CLK,
43: };
```

我们就以 MXC_ESDHC2_CLK 为例，看下uboot代码是如何获取 ESDHC2 外设时钟的。

代码 A 的第23行，其 CONFIG_SYS_FSL_ESDHC_ADDR == USDHC2_BASE_ADDR 为真，所以会调用代码第 24 行的函数。接下来看一下 `mxc_get_clock` 函数的实现：

```c
File: arch\arm\cpu\armv7\mx6\clock.c
174: unsigned int mxc_get_clock(enum mxc_clock clk)
175: {
176: 	switch (clk) {
177: 	case MXC_ARM_CLK:
178: 		return get_mcu_main_clk();
179: 	case MXC_PER_CLK:
180: 		return get_periph_clk();
181: 	case MXC_AHB_CLK:
182: 		return get_ahb_clk();
183: 	case MXC_IPG_CLK:
184: 		return get_ipg_clk();
185: 	case MXC_IPG_PERCLK:
186: 	case MXC_I2C_CLK:
187: 		return get_ipg_per_clk();
188: 	case MXC_UART_CLK:
189: 		return get_uart_clk();
190: 	case MXC_CSPI_CLK:
191: 		return get_cspi_clk();
192: 	case MXC_AXI_CLK:
193: 		return get_axi_clk();
194: 	case MXC_EMI_SLOW_CLK:
195: 		return get_emi_slow_clk();
196: 	case MXC_DDR_CLK:
197: 		return get_mmdc_ch0_clk();
198: 	case MXC_ESDHC_CLK:
199: 		return get_usdhc_clk(0);
200: 	case MXC_ESDHC2_CLK:
201: 		return get_usdhc_clk(1);
202: 	case MXC_ESDHC3_CLK:
203: 		return get_usdhc_clk(2);
204: 	case MXC_ESDHC4_CLK:
205: 		return get_usdhc_clk(3);
206: 	case MXC_SATA_CLK:
207: 		return get_ahb_clk();
208: 	default:
209: 		printf("Unsupported MXC CLK: %d\n", clk);
210: 		break;
211: 	}
212: 
213: 	return 0;
214: }
```

由于调用函数的入参为 MXC_ESDHC2_CLK，所以会调用上面代码的第 201 行 `get_usdhc_clk` 函数，且入参为 1。

```c
File: arch\arm\cpu\armv7\mx6\clock.c --------------------------- (B)
125: static u32 get_usdhc_clk(u32 port)
126: {
127: 	u32 root_freq = 0, usdhc_podf = 0, clk_sel = 0;
128: 	u32 cscmr1 = __raw_readl(&imx_ccm->cscmr1);
129: 	u32 cscdr1 = __raw_readl(&imx_ccm->cscdr1);
130: 
131: 	if (is_mx6ul() || is_mx6ull())
132: 		if (port > 1)
133: 			return 0;
134: 	if (is_mx6sll())
135: 		if (port > 2)
136: 			return 0;
137: 	switch (port) {
138: 	case 0:
139: 		usdhc_podf = (cscdr1 & MXC_CCM_CSCDR1_USDHC1_PODF_MASK) >>
140: 					MXC_CCM_CSCDR1_USDHC1_PODF_OFFSET;
141: 		clk_sel = cscmr1 & MXC_CCM_CSCMR1_USDHC1_CLK_SEL;
142: 
143: 		break;
144: 	case 1:
145: 		usdhc_podf = (cscdr1 & MXC_CCM_CSCDR1_USDHC2_PODF_MASK) >>
146: 					MXC_CCM_CSCDR1_USDHC2_PODF_OFFSET;
147: 		clk_sel = cscmr1 & MXC_CCM_CSCMR1_USDHC2_CLK_SEL;
148: 
149: 		break;
150: 	case 2:
151: 		usdhc_podf = (cscdr1 & MXC_CCM_CSCDR1_USDHC3_PODF_MASK) >>
152: 					MXC_CCM_CSCDR1_USDHC3_PODF_OFFSET;
153: 		clk_sel = cscmr1 & MXC_CCM_CSCMR1_USDHC3_CLK_SEL;
154: 
155: 		break;
156: 	case 3:
157: 		usdhc_podf = (cscdr1 & MXC_CCM_CSCDR1_USDHC4_PODF_MASK) >>
158: 					MXC_CCM_CSCDR1_USDHC4_PODF_OFFSET;
159: 		clk_sel = cscmr1 & MXC_CCM_CSCMR1_USDHC4_CLK_SEL;
160: 
161: 		break;
162: 	default:
163: 		break;
164: 	}
165: 
166: 	if (clk_sel)
167: 		root_freq = mxc_get_pll_pfd(PLL_BUS, 0);
168: 	else
169: 		root_freq = mxc_get_pll_pfd(PLL_BUS, 2);
170: 
171: 	return root_freq / (usdhc_podf + 1);
172: }
```

代码 B 的128 - 129行分别获取寄存器 `cscmr1` 和 `cscdr1` 这两个寄存器的值。这两个寄存器是干什么的？先来看一下变量 `imx_ccm` 的定义是什么？

```c
File: arch\arm\cpu\armv7\mx6\clock.c
25: struct mxc_ccm_reg *imx_ccm = (struct mxc_ccm_reg *)CCM_BASE_ADDR;
```

其中 `CCM_BASE_ADDR` 的值为：`0x020C4000`，而 `cscmr1` 和 `cscdr1` 在结构体 `mxc_ccm_reg` 对应的偏移如下：

![](./images/Snipaste_2024-10-30_22-29-10.png)

所以 &imx_ccm->cscmr1 对应的地址为 0x020C401C，&imx_ccm->cscdr1 对应的地址为 0x020C4024。查看芯片手册如下：

![](./images/Snipaste_2024-10-30_22-32-57.png)

由芯片手册可知，cscmr1 是时钟复用选择寄存器，用于选择外设时钟的来源。cscdr1 则是时钟分频寄存器，用于对外设时钟进行分频。

代码 B 的入参 port = 1，所以会运行代码 B 的第145 - 147行。代码的第145中的宏 `MXC_CCM_CSCDR1_USDHC1_PODF_MASK` 的值为 (7<<16)，而宏 `MXC_CCM_CSCDR1_USDHC1_PODF_OFFSET` 的值为 16。所以我们需要看一下寄存器 cscdr1 的 bit16 - bit18 这三位代表的是什么？

![](./images/Snipaste_2024-10-30_22-42-45.png)

可以看到，bit16 - bit18 代表了USDHC2外设的时钟分频值，代码 B 的145行运行结果得到的 usdhc_podf 就是具体的分频值。

代码 B 的第147行，`MXC_CCM_CSCMR1_USDHC2_CLK_SEL` 的值为 (1<<17)，所以看一下寄存器 cscmr1 的 bit17 的含义是什么：

![](./images/Snipaste_2024-10-30_22-46-48.png)

可以看到 bit17 代表了 usdhc2 外设选择哪个时钟源。其中0代表 PLL2_PFD2, 1代表 PLL2_PFD0。为什么是这两个时钟源，这个需要看一下系统时钟树，局部图如下：

![](./images/Snipaste_2024-10-30_22-50-33.png)

从上图红框中可以看到，usdhc2 可以选择 PFD0 或 PFD2 作为时钟输入源。

不管 usdhc2 外设选择了哪个时钟源，最终都会调用 `mxc_get_pll_pfd` 这个函数，只是入参不同罢了。假如这里选择的时候源是 PFD2，即 cscmr1 寄存器的 bit17 位为 0。那么代码 B 就会调用 `mxc_get_pll_pfd(PLL_BUS, 2)`。

接下来再继续看 `mxc_get_pll_pfd` 函数，代码如下：

```c
File: arch\arm\cpu\armv7\mx6\clock.c --------------------- (C)
096: static u32 mxc_get_pll_pfd(enum pll_clocks pll, int pfd_num)
097: {
098: 	u32 div;
099: 	u64 freq;
100: 
101: 	switch (pll) {
102: 	case PLL_BUS:
103: 		if (!is_mx6ul() && !is_mx6ull()) {
104: 			if (pfd_num == 3) {
105: 				/* No PFD3 on PPL2 */
106: 				return 0;
107: 			}
108: 		}
109: 		div = __raw_readl(&imx_ccm->analog_pfd_528);
110: 		freq = (u64)decode_pll(PLL_BUS, MXC_HCLK);
111: 		break;
112: 	case PLL_USBOTG:
113: 		div = __raw_readl(&imx_ccm->analog_pfd_480);
114: 		freq = (u64)decode_pll(PLL_USBOTG, MXC_HCLK);
115: 		break;
116: 	default:
117: 		/* No PFD on other PLL					     */
118: 		return 0;
119: 	}
120: 
121: 	return lldiv(freq * 18, (div & ANATOP_PFD_FRAC_MASK(pfd_num)) >>
122: 			      ANATOP_PFD_FRAC_SHIFT(pfd_num));
123: }
```

代码 C 的第 96 行的入参 pll 代表了系统的 PLL 时钟源的哪一个组。NXP将时钟源进行了分组，每组时钟都是通过外部晶振进行 PLL 得来的（具体请看 《I.MX6ULL时钟介绍》一文）。uboot 中将这些分组用枚举来表示，如下：

```c
File: arch\arm\cpu\armv7\mx6\clock.c
16: enum pll_clocks {
17: 	PLL_SYS,	/* System PLL */
18: 	PLL_BUS,	/* System Bus PLL*/
19: 	PLL_USBOTG,	/* OTG USB PLL */
20: 	PLL_ENET,	/* ENET PLL */
21: 	PLL_AUDIO,	/* AUDIO PLL */
22: 	PLL_VIDEO,	/* AUDIO PLL */
23: };
```

根据入参会调用代码 C 的第109 - 110 这两行。第109行，根据前面对 imx_ccm 结构的介绍，可以很容易的知道 `&imx_ccm->analog_pfd_528` 的值为 0x20C8100。

![](./images/Snipaste_2024-10-30_23-03-56.png)

该寄存器主要控制 PLL2 的分频值。该寄存器中的各个位含义后面再说，代码的第109行就是先获取该寄存器的值，保存在 div 变量中。

第 110 行中的宏 `MXC_HCLK` 的值为 24000000，就是外设晶振的频率。那就来再看一下函数 `decode_pll` 返回了什么东西吧。

```c
File: arch\arm\cpu\armv7\mx6\clock.c --------------------- (D)
27: static u32 decode_pll(enum pll_clocks pll, u32 infreq)
28: {
29: 	u32 div, test_div, pll_num, pll_denom;
30: 
31: 	switch (pll) {
32: 	case PLL_SYS:
33: 		div = __raw_readl(&imx_ccm->analog_pll_sys);
34: 		div &= BM_ANADIG_PLL_SYS_DIV_SELECT;
35: 
36: 		return (infreq * div) >> 1;
37: 	case PLL_BUS:
38: 		div = __raw_readl(&imx_ccm->analog_pll_528);
39: 		div &= BM_ANADIG_PLL_528_DIV_SELECT;
40: 
41: 		return infreq * (20 + (div << 1));
42: 	case PLL_USBOTG:
43: 		div = __raw_readl(&imx_ccm->analog_usb1_pll_480_ctrl);
44: 		div &= BM_ANADIG_USB1_PLL_480_CTRL_DIV_SELECT;
45: 
46: 		return infreq * (20 + (div << 1));
47: 	case PLL_ENET:
48: 		div = __raw_readl(&imx_ccm->analog_pll_enet);
49: 		div &= BM_ANADIG_PLL_ENET_DIV_SELECT;
50: 
51: 		return 25000000 * (div + (div >> 1) + 1);
52: 	case PLL_AUDIO:
53: 		div = __raw_readl(&imx_ccm->analog_pll_audio);
54: 		if (!(div & BM_ANADIG_PLL_AUDIO_ENABLE))
55: 			return 0;
56: 		/* BM_ANADIG_PLL_AUDIO_BYPASS_CLK_SRC is ignored */
57: 		if (div & BM_ANADIG_PLL_AUDIO_BYPASS)
58: 			return MXC_HCLK;
59: 		pll_num = __raw_readl(&imx_ccm->analog_pll_audio_num);
60: 		pll_denom = __raw_readl(&imx_ccm->analog_pll_audio_denom);
61: 		test_div = (div & BM_ANADIG_PLL_AUDIO_TEST_DIV_SELECT) >>
62: 			BP_ANADIG_PLL_AUDIO_TEST_DIV_SELECT;
63: 		div &= BM_ANADIG_PLL_AUDIO_DIV_SELECT;
64: 		if (test_div == 3) {
65: 			debug("Error test_div\n");
66: 			return 0;
67: 		}
68: 		test_div = 1 << (2 - test_div);
69: 
70: 		return infreq * (div + pll_num / pll_denom) / test_div;
71: 	case PLL_VIDEO:
72: 		div = __raw_readl(&imx_ccm->analog_pll_video);
73: 		if (!(div & BM_ANADIG_PLL_VIDEO_ENABLE))
74: 			return 0;
75: 		/* BM_ANADIG_PLL_AUDIO_BYPASS_CLK_SRC is ignored */
76: 		if (div & BM_ANADIG_PLL_VIDEO_BYPASS)
77: 			return MXC_HCLK;
78: 		pll_num = __raw_readl(&imx_ccm->analog_pll_video_num);
79: 		pll_denom = __raw_readl(&imx_ccm->analog_pll_video_denom);
80: 		test_div = (div & BM_ANADIG_PLL_VIDEO_POST_DIV_SELECT) >>
81: 			BP_ANADIG_PLL_VIDEO_POST_DIV_SELECT;
82: 		div &= BM_ANADIG_PLL_VIDEO_DIV_SELECT;
83: 		if (test_div == 3) {
84: 			debug("Error test_div\n");
85: 			return 0;
86: 		}
87: 		test_div = 1 << (2 - test_div);
88: 
89: 		return infreq * (div + pll_num / pll_denom) / test_div;
90: 	default:
91: 		return 0;
92: 	}
93: 	/* NOTREACHED */
94: }
```

代码 D，根据入参的值，会调用代码 D 的第38 - 41行。第38行获取寄存器 `analog_pll_528` 的值，得了，再看一下 `analog_pll_528` 的定义：

![](./images/Snipaste_2024-10-30_23-11-15.png)

该寄存器是对 PLL 整体上的控制的。第38行就是获取该寄存器的值，保存在 div 变量中。第39行，把 div 与上宏 `BM_ANADIG_PLL_528_DIV_SELECT`，该宏的值为 0x00000001，所以 div 变量的值就是寄存器 `analog_pll_528` 的 bit0 这一位的值，所以来看一下该寄存器的 bit0 这一位的含义：

![](./images/Snipaste_2024-10-30_23-14-35.png)

该位定义了输入频率与输出频率之间的关系。为0时，输出频率等于输入频率的20倍；为1时，输出频率等于输入频率的22倍。由于该位在 reset 时的值为 1，所以这里我们姑且认为 div = 1。

代码 D 的第41行，返回输出频率，此时返回的值为 `(24M * (20 + 1 << 1)) = 528MHz`。

然后返回到代码 C 的第110，即变量 `freq = 528M`。然后接着调用 121 行的 `lldiv`，lldiv函数是处理被除数是64位，除数是32位的除法，lldiv函数返回的就是除法商。

在分析该行时，先看一下入参都是些什么。

`lldiv(freq * 18, (div & ANATOP_PFD_FRAC_MASK(pfd_num)) >> ANATOP_PFD_FRAC_SHIFT(pfd_num));`

第一个参数 freq * 18，经过前面的分析，第一个参数的值就是: 528000000*18; 第二个参数中：

```
NATOP_PFD_FRAC_MASK(pfd_num) = 0x3F << (8 * 2)
ANATOP_PFD_FRAC_SHIFT = 8 * 2
div 经前面分析，它的值就是寄存器 analog_pfd_528 的值
```

所以第二个参数其实就是获取寄存器 analog_pfd_528 的 bit16 - bit21 这6位的值，也即 PLL2 的分频值。

这时再来看一下 analog_pfd_528 寄存器的 bit16 - bit21 的含义：

![](./images/Snipaste_2024-10-30_23-25-46.png)

所以，代码 C 最终返回的值就是上面图片中列出的算法 `528*18/PFD2_FRAC`。

然后代码返回到代码 B 的第 171 行，这里还要再次分频。上面那个分析是 PLL2 的分频，代码 B 的第171 行的分频是 usdhc2 外设的分频，所以把代码 C 返回的 PLL2 的时钟频率再除以 `(usdhc_podf + 1)` 即得到了最终的 usdhc2 外设的时钟频率。


其它外设的时钟频率也是通过调用上述提到的这些函数来获得的。通过不同的入参来获取不同的外设时钟频率。这里不再赘述。

