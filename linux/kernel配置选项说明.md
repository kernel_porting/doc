
[TOC]

# CONFIG_ARM_VIRT_EXT
CONFIG_ARM_VIRT_EXT 配置项启用 ARM 架构中的虚拟化扩展（Virtualization Extensions）。这些扩展允许 ARM 处理器支持硬件辅助的虚拟化，从而提高虚拟机的性能和安全性。

# CONFIG_ARM_LPAE
CONFIG_ARM_LPAE 是一个内核配置选项，用于启用 ARM 架构的 Large Physical Address Extension（LPAE，大物理地址扩展）功能。

```
功能：
    大物理地址支持：LPAE 允许 ARM 处理器支持超过 4GB 的物理内存。传统的 ARMv7 架构最多支持 4GB 的物理地址空间，而 LPAE 扩展了这一限制，使 ARM 处理器能够支持更大的物理内存。
    多级页表：LPAE 引入了多级页表结构，通常为三级或四级页表，以支持更大的地址空间。
```

# CONFIG_XIP_KERNEL
CONFIG_XIP_KERNEL 是一个内核配置选项，用于启用 eXecute In Place (XIP) 功能。XIP 允许内核直接从只读存储器（如闪存）中执行，而不是先将内核加载到 RAM 中再执行。

```
功能
    直接执行：内核代码直接从只读存储器（如闪存）中执行，而不是先复制到 RAM 中。
    节省内存：由于内核代码不需要占用 RAM，可以显著减少系统启动时所需的 RAM 容量。
    快速启动：在某些情况下，XIP 可以加快系统的启动速度，因为减少了将内核从存储器复制到 RAM 的时间。
```

# CONFIG_SMP_ON_UP
CONFIG_SMP_ON_UP 是 Linux 内核的一个配置选项，主要用于控制单处理器系统（UP，Uniprocessor）是否启用对称多处理（SMP，Symmetric Multi-Processing）支持。

# CONFIG_ARM_PATCH_PHYS_VIRT
CONFIG_ARM_PATCH_PHYS_VIRT 是 Linux 内核的一个配置选项，主要用于 ARM 架构。它涉及物理地址和虚拟地址之间的转换和映射。

```
作用
    物理地址和虚拟地址的动态转换：当 CONFIG_ARM_PATCH_PHYS_VIRT 被启用时，内核会在启动过程中动态地修补（patch）某些关键的内核代码和数据结构，以确保物理地址和虚拟地址之间的正确转换。
    灵活性：这种动态修补机制使得内核可以在不同的内存布局和配置下正常工作，提高了内核的灵活性和适应性。
    性能优化：通过动态修补，内核可以避免在运行时频繁进行物理地址和虚拟地址的转换，从而提高性能。
具体实现
    启动过程中的修补：在内核启动早期，内核会读取内存布局信息，并根据这些信息动态地修改某些内核代码和数据结构，使其能够正确地处理物理地址和虚拟地址的转换。
    常量替换：例如，内核可能会将某些常量或宏定义替换为具体的物理地址或虚拟地址，以确保内核代码在运行时能够正确访问内存。
```