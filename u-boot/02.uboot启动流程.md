[TOC]

# 1.编译脚本Makefile
要想了解uboot的启动过程，首先需要知道uboot运行起来后的入口函数在哪？这个要怎么确定？答案就是链接脚本。所以矛盾点便是哪个链接脚本才是uboot编译时被使用的？现在抛出两个问题：

- (1). uboot中有很多链接脚本，在编译时uboot是如何确实要使用哪个链接脚本的？
- (2). uboot支持很多cpu，在编译的时候uboot是怎么选择哪一个cpu的？

在回答这两个问题，从 Makefile 中即可得到答案。因为uboot最终编译得到的文件就是u-boot，所以我们在顶层Makefile中搜索 `u-boot:` 即可找到如下代码：

```
File: Makefile
1255: u-boot:	$(u-boot-init) $(u-boot-main) u-boot.lds FORCE
1256: 	+$(call if_changed,u-boot__)
```

可以看到 u-boot 这个目标文件依赖于 u-boot.lds 这个文件，所以我们需要找到 u-boot.lds 这个文件是哪来的？还是在顶层Makefile中有如下代码：

```
File: Makefile
1399: u-boot.lds: $(LDSCRIPT) prepare FORCE
1400: 	$(call if_changed_dep,cpp_lds)
```

所以 u-boot.lds 文件依赖于 LDSCRIPT，继续查找 LDSCRIPT 是什么东西：

```
File: Makefile
563: ifndef LDSCRIPT
564: 	ifeq ($(wildcard $(LDSCRIPT)),)
565: 		LDSCRIPT := $(srctree)/board/$(BOARDDIR)/u-boot.lds
566: 	endif
567: 	ifeq ($(wildcard $(LDSCRIPT)),)
568: 		LDSCRIPT := $(srctree)/$(CPUDIR)/u-boot.lds
569: 	endif
570: 	ifeq ($(wildcard $(LDSCRIPT)),)
571: 		LDSCRIPT := $(srctree)/arch/$(ARCH)/cpu/u-boot.lds
572: 	endif
573: endif
```

查看顶层Makefile中，我们是没有定义 LDSCRIPT 这个变量的，所以上述的判断为真，所以接下来就依次从下面三个目录中获取链接文件，从上到下其链接脚本越来越通用。

要搞清楚链接脚本的目录，所以还要搞清楚 srctree, BOARDDIR, CPUDIR, ARCH 这几个变量的值。

## 1.1 srctree 变量的值
srctree 可以很容易的从 Makefile 中看到，该变量等于 "."  即当前路径。

## 1.2 BOARDDIR 变量的值
代码中全局搜索 BOARDDIR，可以看到如下代码：

```
File: config.mk
52: ifneq ($(BOARD),)
53: ifdef	VENDOR
54: BOARDDIR = $(VENDOR)/$(BOARD)
55: else
56: BOARDDIR = $(BOARD)
57: endif
58: endif
```

所以 BOARDDIR 的值由 BOARD 和 VENDOR 变量决定，BOARD 变量定义如下：

```
File: config.mk
32: BOARD := $(CONFIG_SYS_BOARD:"%"=%)
```

```
File: config.mk
33: ifneq ($(CONFIG_SYS_VENDOR),)
34: VENDOR := $(CONFIG_SYS_VENDOR:"%"=%)
35: endif
```

而 CONFIG_SYS_BOARD 的值是由我们移植的时候自行配置的，如下图：

![](./images/Snipaste_2024-10-21_22-49-32.png)

所以 BOARD=alpha_mini，VENDOR=freescale。

所以第54行被执行，即 `BOARDDIR=freescale/alpha_mini`。

## 1.3 CPUDIR 变量的值

```
File: config.mk
25: ARCH := $(CONFIG_SYS_ARCH:"%"=%)
26: CPU := $(CONFIG_SYS_CPU:"%"=%)
```

ARCH=arm，CPU=armv7

```
File: config.mk
44: CPUDIR=arch/$(ARCH)/cpu$(if $(CPU),/$(CPU),)
```

所以 `CPUDIR=arch/arm/cpu/armv7`

然后再次回到顶层 Makefile，来确定 LDSCRIPT 变量的值，第565行不存在该文件；第568也不存在该文件，所以最终的 LDSCRIPT 变量的值就是第 571的值，即为 `./arch/arm/cpu/u-boot.lds`

所以其实前面提到的两个问题都有答案了，链接脚本已确定，CPU为 armv7。

所以顶层Makefile的1399行生成的 u-boot.lds 的输入文件就是 `arch/arm/cpu/u-boot.lds`。编译完成后会在根目录下生成 u-boot.lds 文件，它与 `arch/arm/cpu/u-boot.lds` 的文件不同之处在于，u-boot.lds 把 `arch/arm/cpu/u-boot.lds` 文件中的宏定义都去掉了，只保留了真正有效的代码，所以我们分析根目录下的 u-boot.lds 文件是最方便的。

# 2. 链接脚本
打开链接脚本，内容如下(片段)：

```
File: u-boot.lds ----------------------- (A)
04: SECTIONS
05: {
06:  . = 0x00000000;
07:  . = ALIGN(4);
08:  .text :
09:  {
10:   *(.__image_copy_start)
11:   *(.vectors)
12:   arch/arm/cpu/armv7/start.o (.text*)
13:   *(.text*)
14:  }
...
```

从链接脚本可以看到，.text 代码段的第一个位置是 __image_copy_start，该段标记着整个uboot代码段的起始地址，搜索代码可以看到如下代码：

```c
File: arch\arm\lib\sections.c
24: char __image_copy_start[0] __attribute__((section(".__image_copy_start")));
```

可以看到 __image_copy_start[0] 变量就是保存在 .__image_copy_start 这个段中，换句话说，段 .__image_copy_start 中保存的数据就是 __image_copy_start[0]。那段.__image_copy_start 对应的地址是多少？那就需要知道在链接时我们指定的链接地址是多少，因为.__image_copy_start处于链接脚本的首位置，所以我们指定的链接地址是多少，则.__image_copy_start段的地址就是多少。

查看顶层Makefile有如下代码：

```
File: Makefile
819: ifneq ($(CONFIG_SYS_TEXT_BASE),)
820: LDFLAGS_u-boot += -Ttext $(CONFIG_SYS_TEXT_BASE)
821: endif
```

可以看到我们指定的链接地址为 CONFIG_SYS_TEXT_BASE，而我们在代码中确实配置了该宏，如下：

```c
File: include\configs\alpha_mini.h
24: #define CONFIG_SYS_TEXT_BASE	    0x87800000
```

所以 .__image_copy_start 的值即为 0x87800000。(可以从u-boot.map文件中验证)

接下来接着看 (A) 代码片段的第11行，说明把所有的.vectors段都放到.text段中，所以要明白.vectors段是什么，搜索一下该段，可以发现有如下代码：

```c
File: arch\arm\lib\vectors.S
36: 	.section ".vectors", "ax"
```

可以看到在 `arch/arm/lib/vectors.S` 文件中定义了 .vectors 段。上述代码的第36行代表在遇到下一个 .section 指令前，该行以下的所有代码段都被放到了 .vectors 段中，"ax"代表该段的属性，"a"为可分配(allocatable)，"x"为可执行(executable)。

代码 A 的第12行就是把 `arch/arm/cpu/armv7/start.S`的所有 .text 代码段放在 .vectors 段后。

代码 A 的第13行就是把其余所有文件的 .text 代码段放到 `arch/arm/cpu/armv7/start.S`的 .text 段之后。

这上面的代码 A 的片段可以看出整个 uboot 可执行程序的 .text 段的排布规则。

# 3. _start
_start 作为 .vectors 段的起始地址，所以uboot运行起来后就会从这里开始执行。紧接着 _start 标号下面就是定义的中断向量表，中断向量表的第一行为 b reset，即会立即执行 reset 这个函数。目前先不看 reset 这个函数，先看一下 _start 所在的文件是做什么的？

首先定一下基调，该文件就是定义中断向量表的，并实现中断向量表跳转的代码。下面是删除注释/删除宏判断后的该文件的所有源码：

```c
File: arch\arm\lib\vectors.S  -------------------- (B)
...
016: #include <config.h>
...
026: .globl _start
...
036: 	.section ".vectors", "ax"
...
048: _start:
...
054: 	b	reset
055: 	ldr	pc, _undefined_instruction
056: 	ldr	pc, _software_interrupt
057: 	ldr	pc, _prefetch_abort
058: 	ldr	pc, _data_abort
059: 	ldr	pc, _not_used
060: 	ldr	pc, _irq
061: 	ldr	pc, _fiq
...
082: 	.globl	_undefined_instruction
083: 	.globl	_software_interrupt
084: 	.globl	_prefetch_abort
085: 	.globl	_data_abort
086: 	.globl	_not_used
087: 	.globl	_irq
088: 	.globl	_fiq
089: 
090: _undefined_instruction:	.word undefined_instruction
091: _software_interrupt:	.word software_interrupt
092: _prefetch_abort:	.word prefetch_abort
093: _data_abort:		.word data_abort
094: _not_used:		.word not_used
095: _irq:			.word irq
096: _fiq:			.word fiq
097: 
098: 	.balignl 16,0xdeadbeef
...
125: /* IRQ stack memory (calculated at run-time) + 8 bytes */
126: .globl IRQ_STACK_START_IN
127: IRQ_STACK_START_IN:
...
129: 	.word   IRAM_BASE_ADDR + 0x20
...
133: 
134: @
135: @ IRQ stack frame.
136: @
137: #define S_FRAME_SIZE	72
138: 
139: #define S_OLD_R0	68
140: #define S_PSR		64
141: #define S_PC		60
142: #define S_LR		56
143: #define S_SP		52
144: 
145: #define S_IP		48
146: #define S_FP		44
147: #define S_R10		40
148: #define S_R9		36
149: #define S_R8		32
150: #define S_R7		28
151: #define S_R6		24
152: #define S_R5		20
153: #define S_R4		16
154: #define S_R3		12
155: #define S_R2		8
156: #define S_R1		4
157: #define S_R0		0
158: 
159: #define MODE_SVC 0x13
160: #define I_BIT	 0x80
161: 
162: /*
163:  * use bad_save_user_regs for abort/prefetch/undef/swi ...
164:  * use irq_save_user_regs / irq_restore_user_regs for IRQ/FIQ handling
165:  */
166: 
167: 	.macro	bad_save_user_regs
168: 	@ carve out a frame on current user stack
169: 	sub	sp, sp, #S_FRAME_SIZE
170: 	stmia	sp, {r0 - r12}	@ Save user registers (now in svc mode) r0-r12
171: 	ldr	r2, IRQ_STACK_START_IN
172: 	@ get values for "aborted" pc and cpsr (into parm regs)
173: 	ldmia	r2, {r2 - r3}
174: 	add	r0, sp, #S_FRAME_SIZE		@ grab pointer to old stack
175: 	add	r5, sp, #S_SP
176: 	mov	r1, lr
177: 	stmia	r5, {r0 - r3}	@ save sp_SVC, lr_SVC, pc, cpsr
178: 	mov	r0, sp		@ save current stack into r0 (param register)
179: 	.endm
180: 
181: 	.macro	irq_save_user_regs
182: 	sub	sp, sp, #S_FRAME_SIZE
183: 	stmia	sp, {r0 - r12}			@ Calling r0-r12
184: 	@ !!!! R8 NEEDS to be saved !!!! a reserved stack spot would be good.
185: 	add	r8, sp, #S_PC
186: 	stmdb	r8, {sp, lr}^		@ Calling SP, LR
187: 	str	lr, [r8, #0]		@ Save calling PC
188: 	mrs	r6, spsr
189: 	str	r6, [r8, #4]		@ Save CPSR
190: 	str	r0, [r8, #8]		@ Save OLD_R0
191: 	mov	r0, sp
192: 	.endm
193: 
194: 	.macro	irq_restore_user_regs
195: 	ldmia	sp, {r0 - lr}^			@ Calling r0 - lr
196: 	mov	r0, r0
197: 	ldr	lr, [sp, #S_PC]			@ Get PC
198: 	add	sp, sp, #S_FRAME_SIZE
199: 	subs	pc, lr, #4		@ return & move spsr_svc into cpsr
200: 	.endm
201: 
202: 	.macro get_bad_stack
203: 	ldr	r13, IRQ_STACK_START_IN		@ setup our mode stack
204: 
205: 	str	lr, [r13]	@ save caller lr in position 0 of saved stack
206: 	mrs	lr, spsr	@ get the spsr
207: 	str	lr, [r13, #4]	@ save spsr in position 1 of saved stack
208: 	mov	r13, #MODE_SVC	@ prepare SVC-Mode
209: 	@ msr	spsr_c, r13
210: 	msr	spsr, r13	@ switch modes, make sure moves will execute
211: 	mov	lr, pc		@ capture return pc
212: 	movs	pc, lr		@ jump to next instruction & switch modes.
213: 	.endm
214: 
215: 	.macro get_irq_stack			@ setup IRQ stack
216: 	ldr	sp, IRQ_STACK_START
217: 	.endm
218: 
219: 	.macro get_fiq_stack			@ setup FIQ stack
220: 	ldr	sp, FIQ_STACK_START
221: 	.endm
222: 
223: /*
224:  * exception handlers
225:  */
226: 
227: 	.align  5
228: undefined_instruction:
229: 	get_bad_stack
230: 	bad_save_user_regs
231: 	bl	do_undefined_instruction
232: 
233: 	.align	5
234: software_interrupt:
235: 	get_bad_stack
236: 	bad_save_user_regs
237: 	bl	do_software_interrupt
238: 
239: 	.align	5
240: prefetch_abort:
241: 	get_bad_stack
242: 	bad_save_user_regs
243: 	bl	do_prefetch_abort
244: 
245: 	.align	5
246: data_abort:
247: 	get_bad_stack
248: 	bad_save_user_regs
249: 	bl	do_data_abort
250: 
251: 	.align	5
252: not_used:
253: 	get_bad_stack
254: 	bad_save_user_regs
255: 	bl	do_not_used
256: 
257: 
258: 	.align	5
259: irq:
260: 	get_bad_stack
261: 	bad_save_user_regs
262: 	bl	do_irq
263: 
264: 	.align	5
265: fiq:
266: 	get_bad_stack
267: 	bad_save_user_regs
268: 	bl	do_fiq
```

上述代码 (B) 的第54到61行就是定义的中断向量表。

第82到88行声明全局变量，这此行本身是不占用内存空间的。

第98行的 .balignl 是伪命令，其语法格式如下：

```
.balignl <alignment>, <fill>
```

其中 alignment 是对齐值，必须是 2 的幂，且大于等于 4。fill 是填充值，可以是任意值。因为存在对齐操作，所以该条命令可能会占用内存空间。

第129行定义了一个标签为 IRQ_STACK_START_IN，该处地址保存的值为 IRAM_BASE_ADDR + 0x20；查找代码可以看到，我们配置了 IRAM_BASE_ADDR 的值为 0x00900000,这个值的来源可以参数 I.MX6ULL 手册，该值是 I.MX6ULL 的 OCRAM 地址，地址映射如下图：

![](./images/Snipaste_2024-10-18_11-04-13.png)

所以说，这里的意思是，当CPU发生异常时，异常模式下的栈就保存在以 IRQ_STACK_START_IN(0x00900020) 为起始地址的位置处。存疑：这里加 0x20 是为什么？？？
 
第137到157行是宏定义，不占用内存空间。这些宏是代表了IRQ的栈帧，主要是指定IRQ模式下各个寄存器在内存中保存的位置的偏移，有点抽象，后面使用到这些宏时就明白它的作用了。

第159到160行又是两个宏定义，其中MODE_SVC代表了CPU的SVC模式，参考《ARM Cortex-A(armV7)编程手册》，可以看到处理器的模式如下：

![](./images/Snipaste_2024-10-22_22-41-38.png)

SVC对应的编码刚好是 0x13。

I_BIT代表IRQ中断屏蔽位，0x80刚好对应bit7的I。

![](./images/Snipaste_2024-10-22_22-45-04.png)

第167到221行定义了6个宏，主要是处理异常中断。宏可以被调用，所以我们先不看这些宏，先看它们是如何被调用的。

第227到268行就是各个异常中断的入口，实现方式基本一样，我们以 irq 入口为例分析一下。

第258行，.align 5 是对齐，对齐到 2^5^，即32字节对齐。

第259行为 irq 入口。

这里说一下，如果代码运行到第259行，说明现在CPU的模式已经处于IRQ模式了，因为发生异常时ARM处理器会自动执行如下动作:

```
1. 拷贝 CPSR 中的内容到对应异常模式下的 SPSR_<mode>
2. 修改 CPSR 的值
    2.1 修改中断禁止位禁止相应的中断
    2.2 修改模式位进入相应的异常模式(M[4:0])
    2.3 修改状态位进入ARM状态
3. 保存返回地址到对应异常模式下的 LR_<mode>
4. 设置 PC 为相应异常向量(异常向量表对应的地址)
```

上面的4步是ARM处理器发生异常时自动执行的，如果发生的是 IRQ，则上述4步执行完成后就会进入到代码的 259 行。


第260行，调用 get_bad_stack 宏。所以来看一下 get_bad_stack 宏的实现。

第202行：即为 get_bad_stack 宏的名字。

第203行：设置IRQ模式下的栈指针，即 sp=0x00900020。

第205行：保存调用者(异常发生时)的 lr 值到栈中(0x00900020处)。

第206，207行：这两行就是先获取 cpsr 寄存器的值，然后把 cpsr 的值保存到栈中(0x00900024处)

第208到210行：把 spsr 寄存器的低5位设置为 0x13，即准备切换到 SVC 模式。(只有设置 cpsr 才能切换cpu模式)。

第211，212行：宏的正常退出，因为第206行修改了 lr 的值，所以这里要把 lr 的值再恢复回来。注意第212行：`movs pc lr`，这条指令不仅实现了跳转，还隐含地完成了模式切换。因为 spsr 已经被设置为 SVC 模式，所以当 pc 被更新时，处理器会切换到 SVC 模式 (所以说，除了 SVC 模式外，其它异常模式都是转瞬即逝的)。

![](./images/Snipaste_2024-10-23_11-07-57.png)

- 注意，上图中的 spsr_\<irq\> 的值是发生异常前 user 模式下的 cpsr 的值；同样的，lr 的值也是发生异常前 user 模式下的 lr 的值。


接下来再次回到 irq 入口，第260行已经执行完毕，该运行第261行，就是调用 bad_save_user_regs。

第167行：定义了 bad_save_user_regs 宏。执行这个宏的时候，cpu的模式已经切换成了 SVC 模式。

第169行：开辟栈帧，将SP 减去 S_FRAME_SIZE=72 的大小，用于保存该异常模式下的寄存器值。这里我有一个疑问：就是现在已处于 SVC 模式，而各个异常模式下都有属于自己的各自的 sp 寄存器，所以说这里的 sp 应该指的是 sp_\<svc\>，但是 sp_\<svc\> 并没有被赋值啊，它的值是多少？  好吧，查下了资料，说是：`当 CPU 切换到 SVC 模式时，sp_svc 会自动成为当前的 sp。`可以理解为 sp_\<svc\>的初始化。

执行完第169行后，当前的栈空间布局如下图：

![](./images/Snipaste_2024-10-23_11-46-16.png)

第170行：保存用户模式下的 r0 到 r12 寄存器的值。

![](./images/Snipaste_2024-10-23_11-49-53.png)


第171到173行：从 OCRAM 内存中读取出异常模式下的 PC 和 CPSR。所以这时 r2=lr_\<irq\>，r3=spsr_\<irq\>。

第174行：r0=sp+S_FRAME_SIZE，因为第169行的时候，sp先减S_FRAME_SIZE，这里又把sp加上了S_FRAME_SIZE，并把结果赋值给 r0，所以，此时的 r0 又指向了发生异常中的用户的栈帧(即旧的sp)。

第175行：r5 偏移到 S_SP 的位置。此时的 r0, r2, r3, r5 的值如下图：

![](./images/Snipaste_2024-10-23_13-19-58.png)

第176行：把 lr_svc 的值赋值给 r1。？？lr_svc 这个值是多少？？

第177行：依次保存 r0 - r3 这4个寄存器的值到 OCRAM 内存中。这4个寄存器此时保存的值分别为 sp_svc, lr_svc, pc(user模式下，其实准确的说应该是lr), cpsr(user模式下)。

![](./images/Snipaste_2024-10-23_13-37-15.png)

第178行：把 sp 赋值给 r0，此时的 r0 指向了异常模式下的的栈顶。

![](./images/Snipaste_2024-10-23_13-40-45.png)


代码接着向下执行就又回到了 irq 中断向量函数了，最终高用了 do_irq (第262行)去处理具体的中断了。do_irq 函数的入参就是上一步的 r0，即异常模式下保存的栈顶地址。

到这里 vectors.S 这个文件的内容基本都分析完了，其生成的代码都被保存在 .vectors 这个段中，然后由链接脚本控制被链接到整个uboot可执行程序的最开始的地址。

接着我们再次回到代码 (A)，有点久远了，看到代码 (A) 的第 12 行，即链接了 `arch/arm/cpu/armv7/start.S` 这个文件生成的所有 .text 段。所以接下来需要分析下 `arch/arm/cpu/armv7/start.S`这个文件了。

# 4. reset
在第3节刚开始的时候就说明了 uboot 运行起来后就执行了 b reset。所以会运行 reset 这个函数。而 reset 这个函数就定义在　`arch/arm/cpu/armv7/start.S`这个文件中，代码如下：

```c
File: arch\arm\cpu\armv7\start.S
38: reset:
39: 	/* Allow the board to save important registers */
40: 	b	save_boot_params
```

可以看到，reset 内直接又调用了 save_boot_params 函数，其内容如下：

```c
File: arch\arm\cpu\armv7\start.S
115: ENTRY(save_boot_params)
116: 	b	save_boot_params_ret		@ back to my caller
117: ENDPROC(save_boot_params)
```

得，又直接调用了 save_boot_params_ret 函数，其内容如下（删除无效宏定义）：

```c
File: arch\arm\cpu\armv7\start.S  ------------------------ (C)
41: save_boot_params_ret:
...
52: 	/*
53: 	 * disable interrupts (FIQ and IRQ), also set the cpu to SVC32 mode,
54: 	 * except if in HYP mode already
55: 	 */
56: 	mrs	r0, cpsr
57: 	and	r1, r0, #0x1f		@ mask mode bits
58: 	teq	r1, #0x1a		@ test for HYP mode
59: 	bicne	r0, r0, #0x1f		@ clear all mode bits
60: 	orrne	r0, r0, #0x13		@ set SVC mode
61: 	orr	r0, r0, #0xc0		@ disable FIQ and IRQ
62: 	msr	cpsr,r0
...
70: 	/* Set V=0 in CP15 SCTLR register - for VBAR to point to vector */
71: 	mrc	p15, 0, r0, c1, c0, 0	@ Read CP15 SCTLR Register
72: 	bic	r0, #CR_V		@ V = 0
73: 	mcr	p15, 0, r0, c1, c0, 0	@ Write CP15 SCTLR Register
74: 
75: 	/* Set vector address in CP15 VBAR register */
76: 	ldr	r0, =_start
77: 	mcr	p15, 0, r0, c12, c0, 0	@Set VBAR
...
82: 	bl	cpu_init_cp15
...
84: 	bl	cpu_init_crit
...
88: 	bl	_main
```

代码 C 的第56到62行：如果当前的CPU模式不是 HYP 模式的话，则切换CPU模式为 SVC 模式，同时禁止 FIQ 和 IRQ 中断。

代码 C 的第71到73行：设置 SCTLR 寄存器的 CR_V 位为0；CR_V 定义如下：

```c
File: arch\arm\include\asm\system.h
302: #define CR_V	(1 << 13)	/* Vectors relocated to 0xffff0000	*/
```

此位是向量表控制位，当为 0 的时候向量表基地址为 0X00000000，软件可以重定位向量表。为 1 的时候向量表基地址为 0XFFFF0000，软件不能重定位向量表。这里将 V 清零，目的就是为了接下来的向量表重定位。这涉及到 CP15 协处理器的操作，背景知识待补充。。。

代码 C 的第76到77行：设置中断向量表的地址到 CP15 的 VBAR 寄存器中。中断向量表的地址为 _start 标号对应的地址，该标号在 `arch/arm/lib/vectors.S` 文件中，标号下面写的就是中断向量表，_start 的地址就是我们程序的入口地址，即为 0x87800000。

代码 C 的第82行：调用 cpu_init_cp15 函数。

# 5. cpu_init_cp15函数分析

```c
File: arch\arm\cpu\armv7\start.S
127: /*************************************************************************
128:  *
129:  * cpu_init_cp15
130:  *
131:  * Setup CP15 registers (cache, MMU, TLBs). The I-cache is turned on unless
132:  * CONFIG_SYS_ICACHE_OFF is defined.
133:  *
134:  *************************************************************************/
135: ENTRY(cpu_init_cp15)
136: 	/*
137: 	 * Invalidate L1 I/D
138: 	 */
139: 	mov	r0, #0			@ set up for MCR
140: 	mcr	p15, 0, r0, c8, c7, 0	@ invalidate TLBs
141: 	mcr	p15, 0, r0, c7, c5, 0	@ invalidate icache
142: 	mcr	p15, 0, r0, c7, c5, 6	@ invalidate BP array
143: 	mcr     p15, 0, r0, c7, c10, 4	@ DSB
144: 	mcr     p15, 0, r0, c7, c5, 4	@ ISB
145: 
146: 	/*
147: 	 * disable MMU stuff and caches
148: 	 */
149: 	mrc	p15, 0, r0, c1, c0, 0
150: 	bic	r0, r0, #0x00002000	@ clear bits 13 (--V-)
151: 	bic	r0, r0, #0x00000007	@ clear bits 2:0 (-CAM)
152: 	orr	r0, r0, #0x00000002	@ set bit 1 (--A-) Align
153: 	orr	r0, r0, #0x00000800	@ set bit 11 (Z---) BTB
...
157: 	orr	r0, r0, #0x00001000	@ set bit 12 (I) I-cache
...
159: 	mcr	p15, 0, r0, c1, c0, 0
...
196: 	mov	r5, lr			@ Store my Caller
197: 	mrc	p15, 0, r1, c0, c0, 0	@ r1 has Read Main ID Register (MIDR)
198: 	mov	r3, r1, lsr #20		@ get variant field
199: 	and	r3, r3, #0xf		@ r3 has CPU variant
200: 	and	r4, r1, #0xf		@ r4 has CPU revision
201: 	mov	r2, r3, lsl #4		@ shift variant field for combined value
202: 	orr	r2, r4, r2		@ r2 has combined CPU variant + revision
...
304: 	mov	pc, r5			@ back to my caller
305: ENDPROC(cpu_init_cp15)
```

# 6. cpu_init_crit函数分析

```c
File: arch\arm\cpu\armv7\start.S
309: /*************************************************************************
310:  *
311:  * CPU_init_critical registers
312:  *
313:  * setup important registers
314:  * setup memory timing
315:  *
316:  *************************************************************************/
317: ENTRY(cpu_init_crit)
318: 	/*
319: 	 * Jump to board specific initialization...
320: 	 * The Mask ROM will have already initialized
321: 	 * basic memory. Go here to bump up clock rate and handle
322: 	 * wake up conditions.
323: 	 */
324: 	b	lowlevel_init		@ go setup pll,mux,memory
325: ENDPROC(cpu_init_crit)
```

从上面代码可以看出，cpu_init_crit 函数只调用了 lowlevel_init 函数。该函数将真正的初始化具体的 board 了。

# 7. lowlevel_init函数分析
代码如下：

```c
File: arch\arm\cpu\armv7\lowlevel_init.S -------------------------- (D)
24: .pushsection .text.lowlevel_init, "ax"
25: WEAK(lowlevel_init)
26: 	/*
27: 	 * Setup a temporary stack. Global data is not available yet.
28: 	 */
...
32: 	ldr	sp, =CONFIG_SYS_INIT_SP_ADDR
...
34: 	bic	sp, sp, #7 /* 8-byte alignment for ABI compliance */
...
45: 	sub	sp, sp, #GD_SIZE
46: 	bic	sp, sp, #7
47: 	mov	r9, sp
...
50: 	/*
51: 	 * Save the old lr(passed in ip) and the current lr to stack
52: 	 */
53: 	push	{ip, lr}
54: 
55: 	/*
56: 	 * Call the very early init function. This should do only the
57: 	 * absolute bare minimum to get started. It should not:
58: 	 *
59: 	 * - set up DRAM
60: 	 * - use global_data
61: 	 * - clear BSS
62: 	 * - try to start a console
63: 	 *
64: 	 * For boards with SPL this should be empty since SPL can do all of
65: 	 * this init in the SPL board_init_f() function which is called
66: 	 * immediately after this.
67: 	 */
68: 	bl	s_init
69: 	pop	{ip, pc}
70: ENDPROC(lowlevel_init)
71: .popsection
```

上述代码中，可以看到 lowlevel_init 这个函数被 .pushsection 和 .popsection 包围着。关于 .pushsection 和 .popsection 的使用说明如下：

```c
.section .text
.global _start

_start:
    // 一些初始代码

.pushsection .text.lowlevel_init, "ax"
    // 低级别初始化代码 xxx
.popsection

    // 继续主代码

在这个示例中：
1. _start 标签下的代码是主程序的一部分。
2. 使用 .pushsection 切换到 .text.lowlevel_init 段，编写低级别初始化代码。
3. 使用 .popsection 恢复到之前的 .text 段，继续编写主程序的其他部分。
通过这种方式，可以确保不同部分的代码被正确地组织和管理。
示例中的"低级别初始化代码 xxx"就会被存放在段 .text.lowlevel_init 中。
```

回到代码 D，第32行，设置临时栈帧，sp 寄存器为 CONFIG_SYS_INIT_SP_ADDR 的值，该值我们有配置，如下：

```c
File: include\configs\alpha_mini.h
10: #define IRAM_BASE_ADDR             0x00900000  //由芯片手册确定
11: #define IRAM_SIZE                  0x00020000  //芯片手册大小为512KB，但NXP移植时写的是256KB，不知道其余空间是否有它用，待研究
12: #define CONFIG_SYS_INIT_RAM_ADDR	IRAM_BASE_ADDR //初始化阶段时的RAM地址
13: #define CONFIG_SYS_INIT_RAM_SIZE	IRAM_SIZE
14: #define CONFIG_SYS_INIT_SP_OFFSET   (CONFIG_SYS_INIT_RAM_SIZE - GENERATED_GBL_DATA_SIZE)
15: #define CONFIG_SYS_INIT_SP_ADDR     (CONFIG_SYS_INIT_RAM_ADDR + CONFIG_SYS_INIT_SP_OFFSET)
```
上述宏中 GENERATED_GBL_DATA_SIZE 这个值是编译代码时自动生成的，其内容如下：

```c
File: include\generated\generic-asm-offsets.h
9: #define GENERATED_GBL_DATA_SIZE 192 /* (sizeof(struct global_data) + 15) & ~15	@ */
```

所以，其实这里使用的栈空间用的是 OCRAM 的内存。由上面的宏定义可以计算出：

```c
CONFIG_SYS_INIT_SP_ADDR = 0x00900000 + 0x00020000 - 192 = 0x0091FF40;
```

![](./images/Snipaste_2024-10-23_23-57-00.png)

代码 D 的第34行，8字节对齐。

代码 D 的第45行，sp减去了 GD_SIZE，GD_SIZE的大小定义如下：（该宏也是在编译时自动生成的，我们不要修改它）

```c
File: include\generated\generic-asm-offsets.h
11: #define GD_SIZE 192 /* sizeof(struct global_data)	@ */
```

代码 D 的第46行还是8字节对齐。第47行把 sp 的地址赋值给了 r9。程序运行至此 OCRAM 的内存布局如下：

![](./images/Snipaste_2024-10-24_21-19-21.png)

代码 D 的第53行，把 ip 和 lr 压入到当前栈中；然后第68行调用函数 s_init; 第69行将 ip 和 lr 出栈，然后把 lr 赋值给 pc。这里需要注意一下，第53的lr的值是多少？其实 lr 的值等于调用 lowlevel_init 函数的下一条指令的地址，即代码 C 的第88行，也就是 `bl _main`，所以这里就是为了程序能返回所以保存了lr的值，然后利用代码 D 的第69行就可以返回接着执行 _main 函数了。

搞明白了上面的函数返回过程，接下来就要看 s_init 函数了。


# 8. 函数调用流程
![](./images/Snipaste_2024-10-25_23-35-04.png)
