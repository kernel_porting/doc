[TOC]

# Device Tree Binary格式
## DTB整体结构 
经过 DTC 编译，DTS 变成 DTB(Device Tree Blob)的格式。DTB的数据组织如下图所示：

![](./images/Snipaste_2024-12-01_20-56-22.png)

## DTB header
对于 DTB header，其各个成员如下：

|成员|描述|
|:---|:---|
magic|用来识别DTB的。通过这个magic，kernel可以确定bootloader传递的参数block是一个DTB还是tag list
totalsize|DTB的total size。
off_dt_struct|device tree structure block 的 offset 地址。
off_dt_strings|device tree string block 的 offset 地址。
off_mem_rsvmap|offset to memory reserve map。有些系统，我们也许会保留一些memory有特殊用途(例如DTB或者initrd image)，或者在有些DST+ARM的SOC platform上，有写memory被保留用于ARM和DSP进行信息交互。这些保留内存不会进入内存管理系统。
version|DTB版本号。
last_comp_version|兼容版本信息
boot_cpuid_phys|Boot CPU的物理ID。我们在哪一个CPU上booting
dt_strings_size|device tree string block 的 size。和off_dt_strings一起确定了strings block在内存中的位置。
dt_struct_size|device tree structure block 的 size。和off_dt_struct一起确定了device tree structure block在内存中的位置。

下面图片会更直观的表达：

![](./images/Snipaste_2024-12-01_21-24-21.png)

## memory reserve map的格式描述
这个区域包括了若干的 reserve memeory 描述符。每个 reserve memory 描述符是由 address 和 size 组成。其中 address 和 size 都是用 u64 来描述。

## device tree struceture block的格式描述
device tree structure block 区域是由若干的分片组成，每个分片开始位置都是保存了 token，以此来描述该分片的属性和内容。其计有5种token：

- (1) FDT_BEGIN_NODE(0x00000001)。该token描述了一个node的开始位置，紧挨着该token的就是node name(包括 unit address)
- (2) FDT_END_NODE(0x00000002)。该token描述了一个node的结束位置。
- (3) FDT_PROP(0x00000003)。该token描述了一个property的开始位置，该token之后是两个 u32 的数据，分别是 length 和 name offset。length 表示该 property value data 的 size。name offset 表示该属性字符串在 device tree string block 中的偏移值。length 和 name offset 之后就是长度为 length 具体的属性值数据。
- (4) FDT_NOP(0x00000004)。该token表示一个空操作。
- (5) FDT_END(0x00000009)。该token表示一个DTB的结束位置。

上面描述中，第1点对应的数据结构如下：

```c
struct fdt_node_header {
    fdt32_t tag;        //取值FDT_BEGIN_NODE
    char name[0];       //node name
}
```

第3点对应的是属性信息，其数据结构如下：

```c
struct fdt_property {
    fdt32_t tag;        //取值FDT_PROP
    fdt32_t len;        //属性值数据的长度
    fdt_t nameoff;      //属性字符串在 device tree string block 中的偏移值
    char data[0];       //属性值数据
}
```

下图直观的展示了 device tree structure block 的样子：

![](./images/Snipaste_2024-12-01_21-34-08.png)

## device tree string block的格式描述
device tree strings block 定义了各个 node 中使用的属性的字符串表。由于很多属性会出现在多个 node 中，因此，所有的属性字符串组成了一个 string block。这样可以压缩 DTB 的 size。

## 图示总结
设备树文件结构形如：

![](./images/Snipaste_2024-12-01_21-28-41.png)


以上内容参考：

[http://www.wowotech.net/device_model/dt_basic_concept.html](http://www.wowotech.net/device_model/dt_basic_concept.html)

[http://www.wowotech.net/device_model/dt-code-file-struct-parse.html](http://www.wowotech.net/device_model/dt-code-file-struct-parse.html)

## property 结构
struct property 结构体不属于设备树知识的范畴，列在这里只是想简单说明一下内核解析出来设备树中的数据后会以什么样的结构组织这些数据。

设备树文件结构就是 `struct fdt_header, strcut fdt_node_header, struct fdt_property` 三个结构来描述。kernel会根据 Device tree 结构解析出 kernel 能够使用的 struct property 结构体。kernel 根据 Device Tree 中所有的属性解析出数据填充 struct property 结构体。该结构体描述如下：

```c
struct property {
    char *name;                     //property full name
    int length;                     //property value length
    void *value;                    //property value
    struct property *next;          //next property under the same node
    unsigned long _flags;           
    unsigned int unique_id;
    struct bin_attribute attr;      //属性文件，与sysfs文件系统挂接
}
```

